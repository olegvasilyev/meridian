﻿using Meridian.Desktop.Data;
using Meridian.Desktop.Models;
using Meridian.Desktop.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Meridian.Desktop
{
    public partial class MainWindow : Window
    {
        private readonly AppDbContext _context;

        public MainWindow()
        {
            InitializeComponent();

            DataContext = App.ServiceProvider.GetRequiredService<MainWindowViewModel>();
            _context = App.ServiceProvider.GetRequiredService<AppDbContext>();
            var state = _context.WindowStates.FirstOrDefault(x => x.Name.Equals(nameof(MainWindow)));

            if (state != null)
            {
                this.Height = state.Height;
                this.Width = state.Width;
                this.Top = state.Top;
                this.Left = state.Left;
                this.WindowState = state.WindowState;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            var state = _context.WindowStates.FirstOrDefault(x => x.Name.Equals(nameof(MainWindow)));

            if (state != null)
            {
                state.Modified = DateTime.Now.Ticks;
                state.Height = this.Height;
                state.Width = this.Width;
                state.Top = this.Top;
                state.Left = this.Left;
                state.WindowState = this.WindowState;
            }
            else
            {
                _context.WindowStates.Add(new WinState
                {
                    Modified = DateTime.Now.Ticks,
                    Name = nameof(MainWindow),
                    Height = this.Height,
                    Width = this.Width,
                    Top = this.Top,
                    Left = this.Left,
                    WindowState = this.WindowState
                });
            }

            _context.SaveChanges();

            base.OnClosing(e);
        }
    }
}
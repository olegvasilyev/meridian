﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Meridian.Desktop.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GetSettings",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Modified = table.Column<long>(nullable: false),
                    Code = table.Column<string>(maxLength: 32, nullable: false),
                    SettingCode = table.Column<int>(nullable: false),
                    Value = table.Column<string>(maxLength: 1024, nullable: false),
                    ValueType = table.Column<string>(maxLength: 64, nullable: false),
                    Description = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GetSettings", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "WindowStates",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Modified = table.Column<double>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    Height = table.Column<double>(nullable: false),
                    Width = table.Column<double>(nullable: false),
                    Top = table.Column<double>(nullable: false),
                    Left = table.Column<double>(nullable: false),
                    WindowState = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WindowStates", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GetSettings");

            migrationBuilder.DropTable(
                name: "WindowStates");
        }
    }
}

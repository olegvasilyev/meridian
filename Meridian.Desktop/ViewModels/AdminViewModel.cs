﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Meridian.Desktop.Models;
using Meridian.Desktop.Views;
using Meridian.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using T = Telerik.Windows.Controls;

namespace Meridian.Desktop.ViewModels
{
    public class AdminViewModel : ViewModelBase
    {
        private int _selectedIndex;

        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                Set(ref _selectedIndex, value);
            }
        }

        public ObservableCollection<TabItemModel> TabItems { get; set; }

        private readonly UserManager<User> _userManager;

        public RelayCommand<object> LogoutCommand { get; set; }
        public RelayCommand<object> NavigateCommand { get; set; }
        public RelayCommand<ViewType> CloseTabCommand { get; set; }

        public AdminViewModel()
        {
            _userManager = App.ServiceProvider.GetRequiredService<UserManager<User>>();

            LogoutCommand = new RelayCommand<object>((obj) => LogOut(obj));
            NavigateCommand = new RelayCommand<object>((obj) => OpenTab(obj));
            CloseTabCommand = new RelayCommand<ViewType>((vmType) => CloseTab(vmType));
            TabItems = new ObservableCollection<TabItemModel>();
            _selectedIndex = -1;
        }

        public void LogOut(object obj)
        {
            T.DialogParameters dialogParameters = new T.DialogParameters
            {
                Header = Resources.UI.Logout,
                Content = Resources.Messages.AreYouSure,
                Closed = OnClosed
            };

            if (obj is MainWindow)
            {
                dialogParameters.Owner = (MainWindow)obj;
                dialogParameters.DialogStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            }

            T.RadWindow.Confirm(dialogParameters);
        }

        public void CloseTab(ViewType viewModelType)
        {
            var item = TabItems.FirstOrDefault(x => x.ViewModelType.Equals(viewModelType));
            if (item != null) TabItems.Remove(item);
        }

        private async void OnClosed(object sender, T.WindowClosedEventArgs e)
        {
            var dialogResult = e.DialogResult;

            if (dialogResult == true)
            {
                var user = await _userManager.FindByNameAsync(App.ApplicationContext.UserName);
                user.LastActive = DateTime.UtcNow;
                var updateResult = await _userManager.UpdateAsync(user);

                if (updateResult.Succeeded)
                    MessengerInstance.Send<NotifocationModel, MainWindowViewModel>(new NotifocationModel { ViewlType = ViewType.LoginView });
            }
        }

        public void OpenTab(object obj)
        {
            ViewType viewModelType = (ViewType)obj;

            var item = TabItems.FirstOrDefault(x => x.ViewModelType.Equals(viewModelType));

            if (item != null)
            {
                SelectedIndex = TabItems.IndexOf(item);
            }
            else
            {
                var newTabItem = new TabItemModel();

                switch (viewModelType)
                {
                    case ViewType.AdminView:
                        break;

                    case ViewType.CashboxesView:
                        break;

                    case ViewType.CashboxView:
                        break;

                    case ViewType.CashOperationsView:
                        break;

                    case ViewType.CommonSettingsView:
                        break;

                    case ViewType.CostsView:
                        break;

                    case ViewType.CurrenciesView:

                        newTabItem.Name = Resources.UI.Currency;
                        newTabItem.Content = new CurreciesView();
                        newTabItem.ViewModelType = ViewType.CurrenciesView;

                        TabItems.Add(newTabItem);
                        SelectedIndex = TabItems.IndexOf(newTabItem);
                        break;

                    case ViewType.EditEndpointView:
                        break;

                    case ViewType.ExpancesView:
                        break;

                    case ViewType.InvoicesView:
                        break;

                    case ViewType.LeftoversView:
                        break;

                    case ViewType.LoginView:
                        break;

                    case ViewType.MovementsView:
                        break;

                    case ViewType.OrdersView:
                        break;

                    case ViewType.PlacesView:
                        break;

                    case ViewType.ProductsView:

                        newTabItem.Name = Resources.UI.Products;
                        newTabItem.Content = new ProductsView();
                        newTabItem.ViewModelType = ViewType.ProductsView;

                        TabItems.Add(newTabItem);
                        SelectedIndex = TabItems.IndexOf(newTabItem);
                        break;

                    case ViewType.SalesView:
                        break;

                    case ViewType.EmployeesView:
                        break;

                    case ViewType.UsersView:

                        newTabItem.Name = Resources.UI.Users;
                        newTabItem.Content = new UsersView();
                        newTabItem.ViewModelType = ViewType.UsersView;

                        TabItems.Add(newTabItem);
                        SelectedIndex = TabItems.IndexOf(newTabItem);
                        break;

                    case ViewType.WarehousesView:
                        break;

                    case ViewType.WriteOffsView:
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
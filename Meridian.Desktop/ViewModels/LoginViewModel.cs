﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Meridian.Desktop.Models;
using Meridian.Models.Entities;
using Meridian.Models.Models;
using Meridian.Services.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Meridian.Desktop.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private string _login;
        private string _password;
        private string _status;

        public string Login
        {
            get => _login;
            set
            {
                Set(ref _login, value);
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                Set(ref _password, value);
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        public string Status
        {
            get => _status;
            set
            {
                Set(ref _status, value);
            }
        }

        private readonly UserManager<User> _userManager;
        private readonly UserService _usersService;
        private readonly CultureService _cultureService;
        private readonly SettingService _settingService;

        public RelayCommand<object> LoginCommand { get; }

        public LoginViewModel()
        {
            _userManager = App.ServiceProvider.GetRequiredService<UserManager<User>>();
            _usersService = App.ServiceProvider.GetRequiredService<UserService>();
            _cultureService = App.ServiceProvider.GetRequiredService<CultureService>();
            _settingService = App.ServiceProvider.GetRequiredService<SettingService>();

            LoginCommand = new RelayCommand<object>(async (obj) => await LoginAsync(obj), (obj) => CredentialsIsAccepted());
            _status = Resources.UI.LoginDefaulStatus;
        }

        private bool CredentialsIsAccepted()
        {
            return (_login ?? string.Empty).Length > 3 && (_password ?? string.Empty).Length > 3;
        }

        private async Task LoginAsync(object obj)
        {
            var passwordBox = (Telerik.Windows.Controls.RadPasswordBox)obj;
            string password = string.Empty;

            if (passwordBox != null)
                password = passwordBox.Password;

            var user = await _usersService.GetUserByUserName(_login);

            if (user == null)
            {
                Status = Resources.Messages.ErrorUserNotFound;
                return;
            }

            var result = await _userManager.CheckPasswordAsync(user, password);

            if (result)
            {
                var culture = await _cultureService.GetCultureAsync(App.UICulture.Name);
                var currency = await _settingService.GetDefultCurrencyAsync();

                App.ApplicationContext = new ApplicationContext
                {
                    UserName = user.UserName,
                    TimeZoneID = TimeZoneInfo.Local.Id,
                    Culture = culture.Name,
                    Currency = currency
                };

                MessengerInstance.Send<NotifocationModel, MainWindowViewModel>(new NotifocationModel { ViewlType = ViewType.AdminView });
            }
            else
            {
                Status = Resources.Messages.ErrorUserPassword;
            }
        }
    }
}
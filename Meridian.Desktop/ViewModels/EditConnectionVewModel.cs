﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Meridian.Desktop.Helpers;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Meridian.Desktop.ViewModels
{
    public class EditConnectionVewModel : ViewModelBase
    {
        private string _result;
        private string _server;
        private string _databaseName;
        private string _login;
        private string _password;
        private string _defaultConnectionString;
        private string _connectionString;
        private bool _applicable;

        public string Result
        {
            get => _result;
            set
            {
                Set(ref _result, value);
                if (_applicable) Applicable = false;
            }
        }

        public string Server
        {
            get => _server;
            set
            {
                Set(ref _server, value);
                if (_applicable) Applicable = false;
                TestCommand.RaiseCanExecuteChanged();
            }
        }

        public string DatabaseName
        {
            get => _databaseName;
            set
            {
                Set(ref _databaseName, value);
                if (_applicable) Applicable = false;
                TestCommand.RaiseCanExecuteChanged();
            }
        }

        public string Login
        {
            get => _login;
            set
            {
                Set(ref _login, value);
                if (_applicable) Applicable = false;
                TestCommand.RaiseCanExecuteChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                Set(ref _password, value);
                if (_applicable) Applicable = false;
                TestCommand.RaiseCanExecuteChanged();
            }
        }

        public bool Applicable
        {
            get => _applicable;
            set
            {
                Set(ref _applicable, value);
                ApplyCommand.RaiseCanExecuteChanged();
            }
        }

        public RelayCommand<object> TestCommand { get; }

        public RelayCommand<object> ApplyCommand { get; }

        public EditConnectionVewModel()
        {
            _applicable = false;
            _defaultConnectionString = App.Configuration.GetConnectionString(AppConstants.CONNECTIONKEY);
            TestCommand = new RelayCommand<object>(async (obj) => await TestAsync(obj), (obj) => TestIsPossible());
            ApplyCommand = new RelayCommand<object>(async (obj) => await ApplyAsync(obj), (obj) => Applicable);

            _server = "(local)";
            _databaseName = "Meridian";
            _login = "sa";

            Task.Run(async () => await CheckAsync());
        }

        private bool TestIsPossible()
            => (!string.IsNullOrEmpty(_login) && _login.Length >= 2)
            && (!string.IsNullOrEmpty(_password) && _password.Length >= 3)
            && (!string.IsNullOrEmpty(_server) && _server.Length > 3)
            && (!string.IsNullOrEmpty(_databaseName) && _databaseName.Length >= 3);

        private async Task CheckAsync()
        {
            try
            {
                using SqlConnection connection = new SqlConnection(_defaultConnectionString);
                await connection.OpenAsync();
                await connection.CloseAsync();

                Result = Resources.Messages.ConnectiionConfiguredOK;
            }
            catch (Exception)
            {
                Result = Resources.Messages.ErrorDBConnection;
            }
        }

        private async Task TestAsync(object obj)
        {
            var passwordBox = (Telerik.Windows.Controls.RadPasswordBox)obj;
            string password = string.Empty;

            if (passwordBox != null) password = passwordBox.Password;

            var template = Resources.Templates.ConnectionStringTemplate;
            var connectionString = string.Format(template, _server, _databaseName, _login, password);

            try
            {
                using SqlConnection connection = new SqlConnection(connectionString);
                await connection.OpenAsync();
                await connection.CloseAsync();

                _connectionString = connectionString;
                Result = Resources.Messages.ConnectiionConfiguredOK;
                Applicable = true;
            }
            catch (Exception)
            {
                _connectionString = string.Empty;
                Result = Resources.Messages.ErrorDBConnection;
            }
        }

        private async Task ApplyAsync(object obj)
        {
            var passwordBox = (Telerik.Windows.Controls.RadPasswordBox)obj;
            string password = string.Empty;

            if (passwordBox != null) password = passwordBox.Password;

            try
            {
                var template = Resources.Templates.ConnectionStringTemplate;
                var connectionString = string.Format(template, _server, _databaseName, _login, password);

                if (!connectionString.Equals(_defaultConnectionString))
                {
                    var fileContent = await File.ReadAllTextAsync(AppConstants.SETTINGFILE);

                    var jobj = JObject.Parse(fileContent);
                    jobj["ConnectionStrings"][AppConstants.CONNECTIONKEY] = connectionString;

                    var content = Encoding.UTF8.GetBytes(jobj.ToString());

                    await File.WriteAllBytesAsync(AppConstants.SETTINGFILE, content);

                    Result = Resources.Messages.ChangesAppliedRestart;
                }
                else
                {
                    Result = Resources.Messages.ChangesApplied;
                }
            }
            catch (Exception)
            {
                Result = Resources.Messages.ErrorSavingChanges;
            }
        }
    }
}
﻿using GalaSoft.MvvmLight;
using Meridian.Desktop.Models;
using Meridian.Desktop.Views;
using System.Windows.Controls;

namespace Meridian.Desktop.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private UserControl _controlView;

        public UserControl ControlView
        {
            get => _controlView;
            set => Set(ref _controlView, value);
        }

        public MainWindowViewModel()
        {
            ControlView = new LoginView();
            //ControlView = new AdminView();

            MessengerInstance.Register<NotifocationModel>(this, UpdateControlView);
        }

        public void UpdateControlView(NotifocationModel notifocationModel)
        {
            switch (notifocationModel.ViewlType)
            {
                case ViewType.AdminView:
                    ControlView = new AdminView();
                    break;

                case ViewType.CashboxesView:
                    break;

                case ViewType.CashboxView:
                    break;

                case ViewType.CashOperationsView:
                    break;

                case ViewType.CommonSettingsView:
                    break;

                case ViewType.CostsView:
                    break;

                case ViewType.CurrenciesView:
                    break;

                case ViewType.EditEndpointView:
                    break;

                case ViewType.ExpancesView:
                    break;

                case ViewType.InvoicesView:
                    break;

                case ViewType.LeftoversView:
                    break;

                case ViewType.LoginView:
                    ControlView = new LoginView();
                    break;

                case ViewType.MovementsView:
                    break;

                case ViewType.OrdersView:
                    break;

                case ViewType.PlacesView:
                    break;

                case ViewType.ProductsView:
                    break;

                case ViewType.SalesView:
                    break;

                case ViewType.EmployeesView:
                    break;

                case ViewType.UsersView:
                    break;

                case ViewType.WarehousesView:
                    break;

                case ViewType.WriteOffsView:
                    break;

                default:
                    break;
            }
        }
    }
}
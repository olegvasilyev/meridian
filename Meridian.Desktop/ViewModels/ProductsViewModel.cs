﻿using GalaSoft.MvvmLight;
using Meridian.Models.Entities;
using Meridian.Services.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Meridian.Desktop.ViewModels
{
    public class ProductsViewModel : ViewModelBase
    {
        private readonly ProductService _productService;

        public ObservableCollection<Product> Products { get; set; }

        public ProductsViewModel()
        {
            _productService = App.ServiceProvider.GetRequiredService<ProductService>();

            var products = Task.Run(async () => await _productService.GetProducts2()).Result;

            Products = new ObservableCollection<Product>(products);
        }
    }
}
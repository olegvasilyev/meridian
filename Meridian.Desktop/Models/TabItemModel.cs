﻿using System.Windows.Controls;

namespace Meridian.Desktop.Models
{
    public class TabItemModel
    {
        public string Name { get; set; }
        public UserControl Content { get; set; }
        public ViewType ViewModelType { get; set; }
    }
}
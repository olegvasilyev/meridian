﻿using System.ComponentModel.DataAnnotations;
using System.Windows;

namespace Meridian.Desktop.Models
{
    public class WinState
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public double Modified { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        [Required]
        public double Height { get; set; }

        [Required]
        public double Width { get; set; }

        [Required]
        public double Top { get; set; }

        [Required]
        public double Left { get; set; }

        [Required]
        public WindowState WindowState { get; set; }
    }
}
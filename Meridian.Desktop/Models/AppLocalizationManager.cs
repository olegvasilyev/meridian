﻿using Telerik.Windows.Controls;

namespace Meridian.Desktop.Models
{
    public class AppLocalizationManager : LocalizationManager
    {
        public override string GetStringOverride(string key)
        {
            switch (key)
            {
                case "Close":
                    return Resources.UI.Close;

                case "Maximize":
                    return Resources.UI.Maximize;

                case "Restore":
                    return Resources.UI.Restore;

                case "Minimize":
                    return Resources.UI.Minimize;
                
                case "Cancel":
                    return Resources.UI.Cancel;
                                    
                case "GridViewAlwaysVisibleNewRow":
                    return Resources.UI.GridViewAlwaysVisibleNewRow;
                                    
                case "GridViewClearFilter":
                    return Resources.UI.GridViewClearFilter;
                                    
                case "GridViewFilter":
                    return Resources.UI.GridViewFilter;
                                    
                case "GridViewFilterAnd":
                    return Resources.UI.GridViewFilterAnd;
                                    
                case "GridViewFilterContains":
                    return Resources.UI.GridViewFilterContains;
                                    
                case "GridViewFilterDoesNotContain":
                    return Resources.UI.GridViewFilterDoesNotContain;
                                    
                case "GridViewFilterEndsWith":
                    return Resources.UI.GridViewFilterEndsWith;
                                    
                case "GridViewFilterIsContainedIn":
                    return Resources.UI.GridViewFilterIsContainedIn;
                                    
                case "GridViewFilterIsEqualTo":
                    return Resources.UI.GridViewFilterIsEqualTo;
                                    
                case "GridViewFilterIsGreaterThan":
                    return Resources.UI.GridViewFilterIsGreaterThan;
                                    
                case "GridViewFilterIsGreaterThanOrEqualTo":
                    return Resources.UI.GridViewFilterIsGreaterThanOrEqualTo;
                                    
                case "GridViewFilterIsNotContainedIn":
                    return Resources.UI.GridViewFilterIsNotContainedIn;
                                    
                case "GridViewFilterIsLessThan":
                    return Resources.UI.GridViewFilterIsLessThan;
                                    
                case "GridViewFilterIsLessThanOrEqualTo":
                    return Resources.UI.GridViewFilterIsLessThanOrEqualTo;
                                    
                case "GridViewFilterIsNotEqualTo":
                    return Resources.UI.GridViewFilterIsNotEqualTo;
                                    
                case "GridViewFilterMatchCase":
                    return Resources.UI.GridViewFilterMatchCase;
                                    
                case "GridViewFilterOr":
                    return Resources.UI.GridViewFilterOr;
                                    
                case "GridViewFilterSelectAll":
                    return Resources.UI.GridViewFilterSelectAll;
                                    
                case "GridViewFilterShowRowsWithValueThat":
                    return Resources.UI.GridViewFilterShowRowsWithValueThat;
                                    
                case "GridViewFilterStartsWith":
                    return Resources.UI.GridViewFilterStartsWith;
                                    
                case "GridViewFilterIsNull":
                    return Resources.UI.GridViewFilterIsNull;
                                    
                case "GridViewFilterIsNotNull":
                    return Resources.UI.GridViewFilterIsNotNull;
                                    
                case "GridViewFilterIsEmpty":
                    return Resources.UI.GridViewFilterIsEmpty;
                                    
                case "GridViewFilterIsNotEmpty":
                    return Resources.UI.GridViewFilterIsNotEmpty;
                                    
                case "GridViewFilterDistinctValueNull":
                    return Resources.UI.GridViewFilterDistinctValueNull;
                                    
                case "GridViewFilterDistinctValueStringEmpty":
                    return Resources.UI.GridViewFilterDistinctValueStringEmpty;
                                    
                case "GridViewGroupPanelText":
                    return Resources.UI.GridViewGroupPanelText;
                                    
                case "GridViewGroupPanelTopText":
                    return Resources.UI.GridViewGroupPanelTopText;
                                    
                case "GridViewGroupPanelTopTextGrouped":
                    return Resources.UI.GridViewGroupPanelTopTextGrouped;
                                    
                case "GridViewSearchPanelTopText":
                    return Resources.UI.GridViewSearchPanelTopText;
                                                        
                case "GridViewColumnsSelectionButtonTooltip":
                    return Resources.UI.GridViewColumnsSelectionButtonTooltip;

                default:
                    break;
            }

            return base.GetStringOverride(key);
        }
    }
}
﻿using GalaSoft.MvvmLight.Messaging;

namespace Meridian.Desktop.Models
{
    public class NotifocationModel : MessageBase
    {
        public ViewType ViewlType { get; set; }
    }

    public enum ViewType
    {
        AdminView,
        CashboxesView,
        CashboxView,
        CashOperationsView,
        CommonSettingsView,
        CostsView,
        CurrenciesView,
        EditEndpointView,
        ExpancesView,
        InvoicesView,
        LeftoversView,
        LoginView,
        MovementsView,
        OrdersView,
        PlacesView,
        ProductsView,
        SalesView,
        EmployeesView,
        UsersView,
        WarehousesView,
        WriteOffsView
    }
}
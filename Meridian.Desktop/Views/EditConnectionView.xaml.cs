﻿using Meridian.Desktop.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Meridian.Desktop.Views
{
    public partial class EditConnectionView : RadWindow
    {
        public EditConnectionView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<EditConnectionVewModel>();
            InitializeComponent();

            this.PreviewKeyDown += new KeyEventHandler(HandleEsc);
        }

        private void HandleEsc(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) Close();
        }
    }
}
﻿using Meridian.Desktop.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Desktop.Views
{
    public partial class UsersView : UserControl
    {
        public UsersView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<UsersViewModel>();
            InitializeComponent();
        }
    }
}
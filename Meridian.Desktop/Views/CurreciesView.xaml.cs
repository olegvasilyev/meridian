﻿using Meridian.Desktop.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Desktop.Views
{
    public partial class CurreciesView : UserControl
    {
        public CurreciesView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<CurrenciesViewModel>();
            InitializeComponent();
        }
    }
}
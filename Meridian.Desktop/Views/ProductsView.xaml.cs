﻿using Meridian.Desktop.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Desktop.Views
{
    public partial class ProductsView : UserControl
    {
        public ProductsView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<ProductsViewModel>();
            InitializeComponent();
        }
    }
}
﻿using Meridian.Desktop.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Meridian.Desktop.Views
{
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            var vm = App.ServiceProvider.GetRequiredService<LoginViewModel>();
            DataContext = vm;
            
            InitializeComponent();

            var keyBinding = new KeyBinding
            {
                Command = vm.LoginCommand,
                Key = Key.Enter
            };

            keyBinding.CommandParameter = PasswordBox;

            this.InputBindings.Add(keyBinding);
        }

        private void EditConectionString(object sender, RoutedEventArgs e)
        {
            var window = new EditConnectionView() { Owner = Window.GetWindow(this) };
            window.ShowDialog();
        }
    }
}
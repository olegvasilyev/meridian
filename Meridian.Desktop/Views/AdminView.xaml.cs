﻿using Meridian.Desktop.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Desktop.Views
{
    public partial class AdminView : UserControl
    {
        public AdminView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<AdminViewModel>();
            InitializeComponent();
        }
    }
}
﻿using AutoMapper;
using Meridian.Data.Context;
using Meridian.Data.Repositories;
using Meridian.Desktop.Data;
using Meridian.Desktop.Helpers;
using Meridian.Desktop.Models;
using Meridian.Desktop.ViewModels;
using Meridian.Desktop.Views;
using Meridian.Models.Entities;
using Meridian.Models.Models;
using Meridian.Services.Helpers;
using Meridian.Services.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using T = Telerik.Windows.Controls;

namespace Meridian.Desktop
{
    public partial class App : Application
    {
        private static readonly object _locker = new object();
        private static ApplicationContext _appContext;

        public static CultureInfo UICulture { get; private set; }

        public static ApplicationContext ApplicationContext
        {
            get
            {
                lock (_locker)
                {
                    return _appContext ?? new ApplicationContext();
                }
            }
            set
            {
                lock (_locker)
                {
                    _appContext = value;
                }
            }
        }

        public static IConfiguration Configuration { get; private set; }

        public static IServiceProvider ServiceProvider { get; private set; }

        public static string DBFILENAME
        {
            get
            {
                lock (_locker)
                {
                    string appData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

                    var appPath = Path.Combine(appData, "Meridian Desktop");

                    if (!Directory.Exists(appPath))
                        Directory.CreateDirectory(appPath);

                    return Path.Combine(appPath, "App.db");
                }
            }
        }

        public App()
        {
            if (!File.Exists(AppConstants.SETTINGFILE))
            {
                var content = Encoding.UTF8.GetBytes(Desktop.Resources.Templates.AppSetting);
                File.WriteAllBytes(AppConstants.SETTINGFILE, content);
            }

            DispatcherUnhandledException += OnDispatcherUnhandledException;

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(AppConstants.SETTINGFILE, optional: false, reloadOnChange: true);

            Configuration = builder.Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            ServiceProvider = serviceCollection.BuildServiceProvider();

            T.LocalizationManager.Manager = new AppLocalizationManager();
            InitializeComponent();
        }

        public static void SetUICulture(string culture)
        {
            UICulture = new CultureInfo(culture);

            Thread.CurrentThread.CurrentCulture = UICulture;
            Thread.CurrentThread.CurrentUICulture = UICulture;

            Desktop.Resources.UI.Culture = UICulture;
            Desktop.Resources.Messages.Culture = UICulture;
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(x => x.UseSqlServer(Configuration.GetConnectionString("DefaultMSSQLConnection")));

            services.AddDbContext<AppDbContext>(x =>
            {
                x.UseSqlite($"Data Source={DBFILENAME}");
            });

            var builder = services.AddIdentityCore<User>(opt =>
             {
                 opt.Password.RequireDigit = false;
                 opt.Password.RequiredLength = 4;
                 opt.Password.RequireNonAlphanumeric = false;
                 opt.Password.RequireUppercase = false;
             });

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            builder.AddEntityFrameworkStores<DataContext>();
            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();

            services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);

            services.AddSingleton<UnitOfWork>();
            services.AddTransient<UserService>();
            services.AddTransient<CultureService>();
            services.AddTransient<SettingService>();

            // Register all Services.
            services.AddScoped<ProductService>();

            // Register all ViewModels.
            services.AddSingleton<MainWindowViewModel>();
            services.AddTransient<LoginViewModel>();
            services.AddTransient<AdminViewModel>();
            services.AddTransient<EditConnectionVewModel>();
            services.AddTransient<ProductsViewModel>();
            services.AddTransient<CurrenciesViewModel>();
            services.AddTransient<UsersViewModel>();

            // Register all the Windows of the applications.
            //services.AddTransient<MainWindow>();
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            var appContext = ServiceProvider.GetRequiredService<AppDbContext>();
            await appContext.Database.MigrateAsync();

            try
            {
                var dataContext = ServiceProvider.GetRequiredService<DataContext>();
                await dataContext.Database.MigrateAsync();

                //var userManager = ServiceProvider.GetRequiredService<UserManager<User>>();
                //var roleManager = ServiceProvider.GetRequiredService<RoleManager<Role>>();
                //await Meridian.Data.Helpers.Seed.FillDatabaseAsync(dataContext, userManager, roleManager);

                var culture = dataContext.SystemSettings.FirstOrDefault(x => x.SettingType == SettingTypes.DefaultCulture)?.Value ?? CultureInfo.CurrentCulture.Name;

                SetUICulture(culture);

                var window = new MainWindow();
                window.Show();
            }
            catch (Exception)
            {
                SetUICulture(CultureInfo.CurrentCulture.Name);

                var window = new EditConnectionView();
                ((EditConnectionVewModel)window.DataContext).Result = Desktop.Resources.Messages.ErrodDBConnectionMigration;
                window.ShowDialog();
            }

            base.OnStartup(e);
        }

        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(e.Exception.Message);
            sb.AppendLine();
            sb.AppendLine(e.Exception.StackTrace);
            MessageBox.Show(sb.ToString());
            e.Handled = true;
        }
    }
}
﻿namespace Meridian.Desktop.Helpers
{
    public static class AppConstants
    {
        public static string SETTINGFILE = "appSettings.json";
        public static string CONNECTIONKEY = "DefaultMSSQLConnection";
    }
}
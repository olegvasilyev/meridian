﻿using Meridian.Models.DTOs;

namespace Meridian.Models.Models
{
    public class LoginResultModel
    {
        public string Token { get; set; }
        public UserDto User { get; set; }
        public string Culture { get; set; }
        public string Currency { get; set; }
    }
}
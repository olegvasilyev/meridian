﻿using System.Globalization;

namespace Meridian.Models.Models
{
    public class AuthenticationContext
    {
        public string UserName { get; set; }
        public string TimeZoneID { get; set; }
        public string Culture { get; set; }
        public string Currency { get; set; }
    }
}
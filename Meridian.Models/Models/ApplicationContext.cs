﻿using System;

namespace Meridian.Models.Models
{
    public class ApplicationContext
    {
        public string UserName { get; set; }
        public string TimeZoneID { get; set; }
        public string Culture { get; set; }
        public string Currency { get; set; }
    }
}
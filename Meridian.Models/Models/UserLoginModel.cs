﻿using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.Models
{
    public class UserLoginModel
    {
        [Required]
        [MaxLength(16), MinLength(4)]
        public string UserName { get; set; }

        [Required]
        [MaxLength(16), MinLength(4)]
        public string Password { get; set; }
    }
}
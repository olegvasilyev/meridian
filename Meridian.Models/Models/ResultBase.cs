﻿namespace Meridian.Models.Models
{
    public class ResultBase<T>
    {
        public T Data { get; set; }
        public string Message { get; set; }
        public ResultStatus ResultStatus { get; set; } = ResultStatus.Error;

        public void SetSuccess(string message = null)
        {
            ResultStatus = ResultStatus.Success;
            Message = message;
        }

        public void SetWarning(string message = null)
        {
            ResultStatus = ResultStatus.Warning;
            Message = message;
        }

        public void SetError(string message = null)
        {
            ResultStatus = ResultStatus.Error;
            Message = message;
        }
    }

    public enum ResultStatus
    {
        Success = 10,
        Warning = 20,
        Error = 30
    }
}
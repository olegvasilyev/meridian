﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Unit
    {
        public Unit()
        {
            UnitCultures = new HashSet<UnitCulture>();
            Products = new HashSet<Product>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        public virtual ICollection<UnitCulture> UnitCultures { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
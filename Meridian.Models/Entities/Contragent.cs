﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Contragent
    {
        public Contragent()
        {
            Invoices = new HashSet<Invoice>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        [MaxLength(64)]
        public string Email { get; set; }

        [MaxLength(128)]
        public string Contacts { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double? Discount { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}
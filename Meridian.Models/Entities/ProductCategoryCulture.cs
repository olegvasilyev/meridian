﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class ProductCategoryCulture
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public Guid CultureID { get; set; }
        public Guid ProductCategoryID { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public virtual Culture Culture { get; set; }
    }
}
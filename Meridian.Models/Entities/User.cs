﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.Entities
{
    public class User : IdentityUser<Guid>
    {
        public User()
        {
            CompanyEmployees = new HashSet<CompanyEmployee>();
            UserRoles = new HashSet<UserRole>();
        }

        [Required]
        [MaxLength(16)]
        public string FirstName { get; set; }

        [MaxLength(32)]
        public string LastName { get; set; }

        [StringLength(4)]
        public string PIN { get; set; }

        public DateTime Created { get; set; }
        public DateTime? LastActive { get; set; }
        public virtual ICollection<CompanyEmployee> CompanyEmployees { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
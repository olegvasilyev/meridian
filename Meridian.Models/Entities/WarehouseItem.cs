﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class WarehouseItem
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid? ProductID { get; set; }

        [MaxLength(128)]
        public string ProductName { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double Quantity { get; set; }

        public virtual Product Product { get; set; }
    }
}
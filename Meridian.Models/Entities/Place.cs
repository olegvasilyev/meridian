﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Place
    {
        public Place()
        {
            Cashboxes = new HashSet<Cashbox>();
            ProductPlaces = new HashSet<ProductPlace>();
            Tables = new HashSet<Table>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        public Guid CompanyID { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<Cashbox> Cashboxes { get; set; }
        public virtual ICollection<ProductPlace> ProductPlaces { get; set; }
        public virtual ICollection<Table> Tables { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class ProductCategory
    {
        public ProductCategory()
        {
            Products = new HashSet<Product>();
            ChildCategories = new HashSet<ProductCategory>();
            ProductCategoryCultures = new HashSet<ProductCategoryCulture>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public bool IsMenu { get; set; } = false;

        public Guid? ParentCategoryID { get; set; }
        public int? Order { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double? Markup { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double? MinimalAmount { get; set; }

        public Guid? ImageID { get; set; }
        public virtual ProductCategory ParentCategory { get; set; }
        public virtual Image Image { get; set; }
        public virtual ICollection<ProductCategory> ChildCategories { get; set; }
        public virtual ICollection<ProductCategoryCulture> ProductCategoryCultures { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
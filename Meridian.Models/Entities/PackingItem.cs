﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class PackingItem
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid? ProductID { get; set; }

        [MaxLength(128)]
        public string ProductName { get; set; }

        public Guid InvoiceID { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double Price { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double Quantity { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double QuantityLeft { get; set; }

        public virtual Product Product { get; set; }
        public virtual Invoice Invoice { get; set; }
    }
}
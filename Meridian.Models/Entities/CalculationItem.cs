﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class CalculationItem
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid CalculationID { get; set; }
        public Guid? ProductID { get; set; }
        public Guid? WarehouseID { get; set; }

        [MaxLength(128)]
        public string ProductName { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double EntryPrice { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double Quantity { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double LossRatio { get; set; } = 1;

        public virtual Calculation Calculation { get; set; }
        public virtual Product Product { get; set; }
        public virtual Warehouse Warehouse { get; set; }
    }
}
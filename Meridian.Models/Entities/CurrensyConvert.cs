﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class CurrensyConvert
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid SrcID { get; set; }
        public Guid DestID { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double Rate { get; set; }

        public virtual Currency SrcCurrency { get; set; }
        public virtual Currency DestCurrency { get; set; }
    }
}
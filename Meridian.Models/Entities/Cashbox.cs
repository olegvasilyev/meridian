﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Cashbox
    {
        public Cashbox()
        {
            Invoices = new HashSet<Invoice>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        public Guid CurrencyID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid? PlaceID { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double Summ { get; set; } = 0;

        public virtual Currency Currency { get; set; }
        public virtual Company Company { get; set; }
        public virtual Place Place { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}
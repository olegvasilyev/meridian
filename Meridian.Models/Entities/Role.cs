﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Meridian.Models.Entities
{
    public class Role : IdentityRole<Guid>
    {
        public Role()
        {
            UserRoles = new HashSet<UserRole>();
        }

        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
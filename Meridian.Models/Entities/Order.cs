﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Order
    {
        public Order()
        {
            OrderItems = new HashSet<OrderItem>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid? TableID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? PreorderOn { get; set; }

        [MaxLength(256)]
        public string Comment { get; set; }

        public OrderStatus OrderStatus { get; set; }
        public OrderPriority OrderPriority { get; set; }
        public virtual Table Table { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }

    public enum OrderStatus
    {
        Preorder = 10,
        New = 20,
        Preparing = 30,
        Delivering = 40,
        Delivered = 50,
        Closed = 60,
        Rejected = 70,
        Finished = 100
    }

    public enum OrderPriority
    {
        Low = 10,
        Normal = 20,
        High = 30,
        ExtraHigh = 40
    }
}
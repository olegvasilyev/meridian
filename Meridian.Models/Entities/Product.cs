﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Product
    {
        public Product()
        {
            ProductCultures = new HashSet<ProductCulture>();
            ProductCurrencies = new HashSet<ProductCurrency>();
            PackingItems = new HashSet<PackingItem>();
            ProductPlaces = new HashSet<ProductPlace>();
            ProductWarehouses = new HashSet<ProductWarehouse>();
            WarehouseItems = new HashSet<WarehouseItem>();
            Cookings = new HashSet<Cooking>();
            CalculationItems = new HashSet<CalculationItem>();
            OrderItems = new HashSet<OrderItem>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(32)]
        public string Code { get; set; }

        public bool IsMenu { get; set; } //Allow to show in Menu
        public bool IsProtected { get; set; } //Prohibit to sell through fiscal printer

        [Column(TypeName = "decimal(18, 2)")]
        public double MinimalAmount { get; set; } = 0;

        public ProductType ProductType { get; set; }
        public Guid UnitID { get; set; }
        public Guid? ProductCategoryID { get; set; }
        public Guid? ImageID { get; set; }
        public virtual Unit Unit { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public virtual Image Image { get; set; }
        public virtual ICollection<ProductCulture> ProductCultures { get; set; }
        public virtual ICollection<ProductCurrency> ProductCurrencies { get; set; }
        public virtual ICollection<PackingItem> PackingItems { get; set; }
        public virtual ICollection<ProductPlace> ProductPlaces { get; set; }
        public virtual ICollection<ProductWarehouse> ProductWarehouses { get; set; }
        public virtual ICollection<WarehouseItem> WarehouseItems { get; set; }
        public virtual ICollection<Cooking> Cookings { get; set; }
        public virtual ICollection<CalculationItem> CalculationItems { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }

    public enum ProductType
    {
        Product = 10,
        Dish = 20,
        Portion = 30
    }
}
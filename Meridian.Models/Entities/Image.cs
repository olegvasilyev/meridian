﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Image
    {
        public Image()
        {
            Companies = new HashSet<Company>();
            Cultures = new HashSet<Culture>();
            Products = new HashSet<Product>();
            ProductCategories = new HashSet<ProductCategory>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(256)]
        public string OriginalName { get; set; }

        [MaxLength(256)]
        public string LargeName { get; set; }

        [MaxLength(256)]
        public string MediumName { get; set; }

        [MaxLength(256)]
        public string ThumbnailName { get; set; }

        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Culture> Cultures { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class ProductCulture
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid ProductID { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public Guid CultureID { get; set; }

        [MaxLength(32)]
        public string PrinterName { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public virtual Product Product { get; set; }
        public virtual Culture Culture { get; set; }
    }
}
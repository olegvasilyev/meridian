﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Cooking
    {
        public Cooking()
        {
            Calculations = new HashSet<Calculation>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [MaxLength(256)]
        public string Comment { get; set; }

        public Guid ProductID { get; set; }
        public Guid KitchenID { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public OrderPriority OrderPriority { get; set; }
        public virtual Product Product { get; set; }
        public virtual Kitchen Kitchen { get; set; }
        public virtual ICollection<Calculation> Calculations { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class SystemSetting
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string Code { get; set; }

        [Required]
        public SettingTypes SettingType { get; set; }

        [Required]
        [MaxLength(128)]
        public string Value { get; set; }

        [Required]
        [MaxLength(128)]
        public string ValueType { get; set; }

        public string Description { get; set; }
    }

    public enum SettingTypes
    {
        DefaultCulture = 10,
        DefaultCurrency = 20
    }
}
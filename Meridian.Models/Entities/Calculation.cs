﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Calculation
    {
        public Calculation()
        {
            CalculationItems = new HashSet<CalculationItem>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid? CookingID { get; set; }

        [MaxLength(1024)]
        public string Comment { get; set; }

        public int Quantity { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double? Summ { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double? Weight { get; set; }

        public virtual Cooking Cooking { get; set; }
        public virtual ICollection<CalculationItem> CalculationItems { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class ProductCurrency
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double EntryPrice { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double? RetailPrice { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double? Markup { get; set; }

        public Guid ProductID { get; set; }
        public Guid CurrencyID { get; set; }
        public virtual Product Product { get; set; }
        public virtual Currency Currency { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Kitchen
    {
        public Kitchen()
        {
            Cookings = new HashSet<Cooking>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public Guid CompanyID { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<Cooking> Cookings { get; set; }
    }
}
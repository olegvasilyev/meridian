﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Culture
    {
        public Culture()
        {
            ProductCategoryCultures = new HashSet<ProductCategoryCulture>();
            ProductCultures = new HashSet<ProductCulture>();
            UnitCultures = new HashSet<UnitCulture>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(16)]
        public string Name { get; set; }

        [Required]
        [MaxLength(64)]
        public string DisplayName { get; set; }

        public Guid? ImageID { get; set; }
        public virtual Image Image { get; set; }
        public virtual ICollection<ProductCategoryCulture> ProductCategoryCultures { get; set; }
        public virtual ICollection<ProductCulture> ProductCultures { get; set; }
        public virtual ICollection<UnitCulture> UnitCultures { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Currency
    {
        public Currency()
        {
            Cashboxes = new HashSet<Cashbox>();
            SrcCurrencies = new HashSet<CurrensyConvert>();
            DestCurrencies = new HashSet<CurrensyConvert>();
            ProductCurrencies = new HashSet<ProductCurrency>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string EnglishName { get; set; }

        [Required]
        [MaxLength(32)]
        public string NativeName { get; set; }

        public char CurrencySymbol { get; set; }
        public virtual ICollection<Cashbox> Cashboxes { get; set; }
        public virtual ICollection<CurrensyConvert> SrcCurrencies { get; set; }
        public virtual ICollection<CurrensyConvert> DestCurrencies { get; set; }
        public virtual ICollection<ProductCurrency> ProductCurrencies { get; set; }
    }
}
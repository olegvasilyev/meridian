﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Company
    {
        public Company()
        {
            CompanyEmployees = new HashSet<CompanyEmployee>();
            Places = new HashSet<Place>();
            Kitchens = new HashSet<Kitchen>();
            Cashboxes = new HashSet<Cashbox>();
            Warehouses = new HashSet<Warehouse>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        [MaxLength(128)]
        public string Contacts { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public Guid? ImageID { get; set; }
        public virtual Image Image { get; set; }
        public virtual ICollection<CompanyEmployee> CompanyEmployees { get; set; }
        public virtual ICollection<Place> Places { get; set; }
        public virtual ICollection<Kitchen> Kitchens { get; set; }
        public virtual ICollection<Cashbox> Cashboxes { get; set; }
        public virtual ICollection<Warehouse> Warehouses { get; set; }
    }
}
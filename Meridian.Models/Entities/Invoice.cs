﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class Invoice
    {
        public Invoice()
        {
            PackingItems = new HashSet<PackingItem>();
            Invoices = new HashSet<Invoice>();
            WarehouseInvoices = new HashSet<WarehouseInvoice>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid? ContragentID { get; set; }

        [MaxLength(64)]
        public string ContragentName { get; set; }

        public Guid? CashboxID { get; set; }
        public Guid? BasedOnInvoiceID { get; set; }

        [MaxLength(32)]
        public string CahsboxName { get; set; }

        [MaxLength(256)]
        public string Comment { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public double Total { get; set; }

        public InvoiceType InvoiceType { get; set; }
        public PaymentType PaymentType { get; set; }
        public virtual Invoice BasedOnInvoice { get; set; }
        public virtual Contragent Contragent { get; set; }
        public virtual Cashbox Cashbox { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<PackingItem> PackingItems { get; set; }
        public virtual ICollection<WarehouseInvoice> WarehouseInvoices { get; set; }
    }

    public enum InvoiceType
    {
        Incoming = 10,
        Outgoing = 20,
        Return = 30
    }

    public enum PaymentType
    {
        Cash = 10,
        Cashless = 20,
        CashOrder = 30
    }
}
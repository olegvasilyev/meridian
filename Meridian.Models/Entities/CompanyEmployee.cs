﻿using System;

namespace Meridian.Models.Entities
{
    public class CompanyEmployee
    {
        public Guid UserID { get; set; }
        public Guid CompanyID { get; set; }
        public virtual Company Company { get; set; }
        public virtual User User { get; set; }
    }
}
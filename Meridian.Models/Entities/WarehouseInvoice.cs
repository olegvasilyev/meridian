﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class WarehouseInvoice
    {
        public WarehouseInvoice()
        {
            WarehouseItems = new HashSet<WarehouseItem>();
        }

        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [MaxLength(128)]
        public string Comment { get; set; }

        public Guid? WarehouseFromID { get; set; }

        [MaxLength(64)]
        public string WarehouseFromName { get; set; }

        public Guid? WarehouseToID { get; set; }
        public Guid? BasedOnInvoiceID { get; set; }

        [MaxLength(64)]
        public string WarehouseToName { get; set; }

        public virtual Warehouse WarehouseFrom { get; set; }
        public virtual Warehouse WarehouseTo { get; set; }
        public virtual Invoice BasedOnInvoice { get; set; }

        public virtual ICollection<WarehouseItem> WarehouseItems { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meridian.Models.Entities
{
    public class ProductPlace
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid ProductID { get; set; }
        public Guid PlaceID { get; set; }
        public virtual Product Product { get; set; }
        public virtual Place Place { get; set; }
    }
}
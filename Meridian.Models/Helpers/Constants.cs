﻿namespace Meridian.Models.Helpers
{
    public static class Constants
    {
        public static string ADMINISTRATOR = "Administrator";
        public static string ACCOUNTANT = "Accountant";
        public static string CASHIER = "Cashier";
        public static string WAITER = "Waiter";
        public static string COOK = "Cook";
        public static string CUSTOMER = "Customer";
        public static string[] ROLES = { ADMINISTRATOR, ACCOUNTANT, CASHIER, WAITER, COOK, CUSTOMER };
        
        public static string DEFAULTCURRENCY = "USD";
    }
}
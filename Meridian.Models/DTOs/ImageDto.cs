﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ImageDto
    {
        public ImageDto()
        {
            CompanyDtos = new HashSet<CompanyDto>();
            CultureDtos = new HashSet<CultureDto>();
            ProductDtos = new HashSet<ProductDto>();
            ProductCategoryDtos = new HashSet<ProductCategoryDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(256)]
        public string OriginalName { get; set; }

        [MaxLength(256)]
        public string LargeName { get; set; }

        [MaxLength(256)]
        public string MediumName { get; set; }

        [MaxLength(256)]
        public string ThumbnailName { get; set; }

        public ICollection<CompanyDto> CompanyDtos { get; set; }
        public ICollection<CultureDto> CultureDtos { get; set; }
        public ICollection<ProductDto> ProductDtos { get; set; }
        public ICollection<ProductCategoryDto> ProductCategoryDtos { get; set; }
    }
}
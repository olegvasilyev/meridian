﻿namespace Meridian.Models.DTOs
{
    public class UserRoleDto
    {
        public UserDto UserDto { get; set; }
        public RoleDto RoleDto { get; set; }
    }
}
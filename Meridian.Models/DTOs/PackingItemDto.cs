﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class PackingItemDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid? ProductID { get; set; }

        [MaxLength(128)]
        public string ProductName { get; set; }

        public Guid InvoiceID { get; set; }
        public double Price { get; set; }
        public double Quantity { get; set; }
        public double QuantityLeft { get; set; }
        public ProductDto ProductDto { get; set; }
        public InvoiceDto InvoiceDto { get; set; }
    }
}
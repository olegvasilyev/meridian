﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ProductCurrencyDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }
        public string ModifiedBy { get; set; }
        public double EntryPrice { get; set; }
        public double? RetailPrice { get; set; }
        public double? Markup { get; set; }
        public Guid ProductID { get; set; }
        public Guid CurrencyID { get; set; }
        public ProductDto ProductDto { get; set; }
        public CurrencyDto CurrencyDto { get; set; }
    }
}
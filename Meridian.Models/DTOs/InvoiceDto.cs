﻿using Meridian.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class InvoiceDto
    {
        public InvoiceDto()
        {
            PackingItemDtos = new HashSet<PackingItemDto>();
            InvoiceDtos = new HashSet<InvoiceDto>();
            WarehouseInvoiceDtos = new HashSet<WarehouseInvoiceDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid? ContragentID { get; set; }

        [MaxLength(64)]
        public string ContragentName { get; set; }

        public Guid? CashboxID { get; set; }
        public Guid? BasedOnInvoiceID { get; set; }

        [MaxLength(32)]
        public string CahsboxName { get; set; }

        [MaxLength(256)]
        public string Comment { get; set; }

        public double Total { get; set; }

        public InvoiceType InvoiceType { get; set; }
        public PaymentType PaymentType { get; set; }
        public InvoiceDto BasedOnInvoice { get; set; }
        public ContragentDto Contragent { get; set; }
        public CashboxDto Cashbox { get; set; }
        public ICollection<InvoiceDto> InvoiceDtos { get; set; }
        public ICollection<PackingItemDto> PackingItemDtos { get; set; }
        public ICollection<WarehouseInvoiceDto> WarehouseInvoiceDtos { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CashboxDto
    {
        public CashboxDto()
        {
            InvoiceDtos = new HashSet<InvoiceDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        public Guid CurrencyID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid? PlaceID { get; set; }
        public double Summ { get; set; }
        public CurrencyDto Currency { get; set; }
        public CompanyDto Company { get; set; }
        public PlaceDto Place { get; set; }
        public ICollection<InvoiceDto> InvoiceDtos { get; set; }
    }
}
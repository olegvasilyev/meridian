﻿using Meridian.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CreateProductDto
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(32)]
        public string Code { get; set; }

        public bool IsMenu { get; set; }
        public bool IsProtected { get; set; }
        public double? EntryPrice { get; set; }
        public double? RetailPrice { get; set; }
        public double? Markup { get; set; }
        public double MinimalAmount { get; set; }
        public ProductType ProductType { get; set; }
        public Guid UnitID { get; set; }
        public Guid? ProductCategoryID { get; set; }
        public Guid? ImageID { get; set; }
        public List<CreateProductCultureDto> CreateProductCultureDtos { get; set; }
        public List<CreateProductWarehouseDto> CreateProductWarehouseDtos { get; set; }
    }
}
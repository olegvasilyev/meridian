﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ProductCategoryDto
    {
        public ProductCategoryDto()
        {
            ProductDtos = new HashSet<ProductDto>();
            ChildCategorieDtos = new HashSet<ProductCategoryDto>();
            ProductCategoryCultureDtos = new HashSet<ProductCategoryCultureDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public bool IsMenu { get; set; } = false;
        public Guid? ParentCategoryID { get; set; }
        public int? Order { get; set; }
        public double? Markup { get; set; }
        public double? MinimalAmount { get; set; }
        public Guid? ImageID { get; set; }
        public ProductCategoryDto ParentCategoryDto { get; set; }
        public ImageDto Image { get; set; }
        public ICollection<ProductCategoryDto> ChildCategorieDtos { get; set; }
        public ICollection<ProductCategoryCultureDto> ProductCategoryCultureDtos { get; set; }
        public ICollection<ProductDto> ProductDtos { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ProductPlaceDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid ProductID { get; set; }
        public Guid PlaceID { get; set; }
        public ProductDto ProductDto { get; set; }
        public PlaceDto PlaceDto { get; set; }
    }
}
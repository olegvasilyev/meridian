﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Meridian.Models.DTOs
{
    public class CreateProductCultureDto
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public Guid CultureID { get; set; }

        [MaxLength(32)]
        public string PrinterName { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }
    }
}

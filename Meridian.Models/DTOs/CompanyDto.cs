﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CompanyDto
    {
        public CompanyDto()
        {
            CompanyEmploeeDtos = new HashSet<CompanyEmploeeDto>();
            PlaceDtos = new HashSet<PlaceDto>();
            KitchenDtos = new HashSet<KitchenDto>();
            CashboxDtos = new HashSet<CashboxDto>();
            WarehouseDtos = new HashSet<WarehouseDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        [MaxLength(128)]
        public string Contacts { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public Guid? ImageID { get; set; }
        public ImageDto Image { get; set; }
        public ICollection<CompanyEmploeeDto> CompanyEmploeeDtos { get; set; }
        public ICollection<PlaceDto> PlaceDtos { get; set; }
        public ICollection<KitchenDto> KitchenDtos { get; set; }
        public ICollection<CashboxDto> CashboxDtos { get; set; }
        public ICollection<WarehouseDto> WarehouseDtos { get; set; }
    }
}
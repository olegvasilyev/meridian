﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CurrencyDto
    {
        public CurrencyDto()
        {
            Cashboxes = new HashSet<CashboxDto>();
            SrcCurrencies = new HashSet<CurrencyConvertDto>();
            DestCurrencies = new HashSet<CurrencyConvertDto>();
            ProductCurrencyDtos = new HashSet<ProductCurrencyDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string EnglishName { get; set; }

        [Required]
        [MaxLength(32)]
        public string NativeName { get; set; }

        public char CurrencySymbol { get; set; }
        public ICollection<CashboxDto> Cashboxes { get; set; }
        public ICollection<CurrencyConvertDto> SrcCurrencies { get; set; }
        public ICollection<CurrencyConvertDto> DestCurrencies { get; set; }
        public ICollection<ProductCurrencyDto> ProductCurrencyDtos { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CalculationItemDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid CalculationID { get; set; }
        public Guid? ProductID { get; set; }
        public Guid? WarehouseID { get; set; }

        [MaxLength(128)]
        public string ProductName { get; set; }

        public double EntryPrice { get; set; }
        public double Quantity { get; set; }
        public double LossRatio { get; set; } = 1;
        public CalculationDto CalculationDto { get; set; }
        public ProductDto ProductDto { get; set; }
        public WarehouseDto WarehouseDto { get; set; }
    }
}
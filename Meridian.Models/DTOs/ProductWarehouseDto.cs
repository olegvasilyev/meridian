﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ProductWarehouseDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid ProductID { get; set; }
        public Guid WarehousesID { get; set; }
        public double Quantity { get; set; }
        public ProductDto ProductDto { get; set; }
        public WarehouseDto WarehouseDto { get; set; }
    }
}
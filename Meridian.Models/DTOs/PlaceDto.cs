﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class PlaceDto
    {
        public PlaceDto()
        {
            CashboxDtos = new HashSet<CashboxDto>();
            ProductPlaceDtos = new HashSet<ProductPlaceDto>();
            TableDtos = new HashSet<TableDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        public Guid CompanyID { get; set; }
        public CompanyDto Company { get; set; }
        public ICollection<CashboxDto> CashboxDtos { get; set; }
        public ICollection<ProductPlaceDto> ProductPlaceDtos { get; set; }
        public ICollection<TableDto> TableDtos { get; set; }
    }
}
﻿using System;

namespace Meridian.Models.DTOs
{
    public class CreateProductWarehouseDto
    {
        public Guid WarehousesID { get; set; }
        public double Quantity { get; set; }
    }
}
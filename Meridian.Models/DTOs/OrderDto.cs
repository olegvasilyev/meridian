﻿using Meridian.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class OrderDto
    {
        public OrderDto()
        {
            OrderItemDtos = new HashSet<OrderItemDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid? TableID { get; set; }
        public DateTime? PreorderOn { get; set; }

        [MaxLength(256)]
        public string Comment { get; set; }

        public OrderStatus OrderStatus { get; set; }
        public OrderPriority OrderPriority { get; set; }
        public TableDto Table { get; set; }
        public ICollection<OrderItemDto> OrderItemDtos { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class OrderItemDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid OrderID { get; set; }
        public Guid ProductID { get; set; }
        public double Quantity { get; set; }
        public OrderDto Order { get; set; }
        public ProductDto Product { get; set; }
    }
}
﻿using Meridian.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ProductDto
    {
        public ProductDto()
        {
            ProductCultureDtos = new HashSet<ProductCultureDto>();
            ProductCurrencyDtos = new HashSet<ProductCurrencyDto>();
            PackingItemDtos = new HashSet<PackingItemDto>();
            ProductPlaceDtos = new HashSet<ProductPlaceDto>();
            ProductWarehouseDtos = new HashSet<ProductWarehouseDto>();
            WarehouseItemDtos = new HashSet<WarehouseItemDto>();
            CookingDtos = new HashSet<CookingDto>();
            CalculationItemDtos = new HashSet<CalculationItemDto>();
            OrderItemDtos = new HashSet<OrderItemDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(32)]
        public string Code { get; set; }

        public bool IsMenu { get; set; }
        public bool IsProtected { get; set; }
        public double MinimalAmount { get; set; } = 0;
        public ProductType ProductType { get; set; }
        public Guid UnitID { get; set; }
        public Guid? ProductCategoryID { get; set; }
        public Guid? ImageID { get; set; }
        public UnitDto Unit { get; set; }
        public ProductCategoryDto ProductCategoryDto { get; set; }
        public ImageDto ImageDto { get; set; }
        public ICollection<ProductCultureDto> ProductCultureDtos { get; set; }
        public ICollection<ProductCurrencyDto> ProductCurrencyDtos { get; set; }
        public ICollection<PackingItemDto> PackingItemDtos { get; set; }
        public ICollection<ProductPlaceDto> ProductPlaceDtos { get; set; }
        public ICollection<ProductWarehouseDto> ProductWarehouseDtos { get; set; }
        public ICollection<WarehouseItemDto> WarehouseItemDtos { get; set; }
        public ICollection<CookingDto> CookingDtos { get; set; }
        public ICollection<CalculationItemDto> CalculationItemDtos { get; set; }
        public ICollection<OrderItemDto> OrderItemDtos { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ContragentDto
    {
        public ContragentDto()
        {
            InvoiceDtos = new HashSet<InvoiceDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        [MaxLength(64)]
        public string Email { get; set; }

        [MaxLength(128)]
        public string Contacts { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public double? Discount { get; set; }
        public ICollection<InvoiceDto> InvoiceDtos { get; set; }
    }
}
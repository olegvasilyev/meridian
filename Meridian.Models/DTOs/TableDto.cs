﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class TableDto
    {
        public TableDto()
        {
            OrderDtos = new HashSet<OrderDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public Guid PlaceID { get; set; }
        public int NumberOfSeats { get; set; }
        public PlaceDto Place { get; set; }
        public ICollection<OrderDto> OrderDtos { get; set; }
    }
}
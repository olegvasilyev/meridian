﻿using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class RegisterUserDto
    {
        [Required]
        [MaxLength(16)]
        public string UserName { get; set; }

        [Required]
        [MaxLength(16)]
        public string Password { get; set; }

        [Required]
        [MaxLength(16)]
        public string FirstName { get; set; }

        [MaxLength(32)]
        public string LastName { get; set; }

        [MaxLength(64)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [MaxLength(16)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}
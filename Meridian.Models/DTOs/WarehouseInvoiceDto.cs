﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class WarehouseInvoiceDto
    {
        public WarehouseInvoiceDto()
        {
            WarehouseItemDtos = new HashSet<WarehouseItemDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [MaxLength(128)]
        public string Comment { get; set; }

        public Guid? WarehouseFromID { get; set; }

        [MaxLength(64)]
        public string WarehouseFromName { get; set; }

        public Guid? WarehouseToID { get; set; }
        public Guid? BasedOnInvoiceID { get; set; }

        [MaxLength(64)]
        public string WarehouseToName { get; set; }

        public WarehouseDto WarehouseFrom { get; set; }
        public WarehouseDto WarehouseTo { get; set; }
        public InvoiceDto BasedOnInvoice { get; set; }
        public ICollection<WarehouseItemDto> WarehouseItemDtos { get; set; }
    }
}
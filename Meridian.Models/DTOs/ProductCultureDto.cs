﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ProductCultureDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid ProductID { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public Guid CultureID { get; set; }

        [MaxLength(32)]
        public string PrinterName { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public ProductDto ProductDto { get; set; }
        public CultureDto CultureDto { get; set; }
    }
}
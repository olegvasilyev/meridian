﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CreateUserDto
    {
        [Required]
        [MaxLength(16)]
        public string UserName { get; set; }

        [Required]
        [MaxLength(16)]
        public string Password { get; set; }

        [Required]
        [MaxLength(16)]
        public string FirstName { get; set; }

        [MaxLength(32)]
        public string LastName { get; set; }

        [StringLength(4)]
        public string PIN { get; set; }

        public List<string> Roles { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CurrencyConvertDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public Guid SrcID { get; set; }
        public Guid DestID { get; set; }
        public double Rate { get; set; }
        public CurrencyDto SrcCurrencyDto { get; set; }
        public CurrencyDto DestCurrencyDto { get; set; }
    }
}
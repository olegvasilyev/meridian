﻿using System;
using System.Collections.Generic;

namespace Meridian.Models.DTOs
{
    public class RoleDto
    {
        public RoleDto()
        {
            UserRoleDtos = new HashSet<UserRoleDto>();
        }

        public Guid ID { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public ICollection<UserRoleDto> UserRoleDtos { get; set; }
    }
}
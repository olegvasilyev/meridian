﻿using System;
using System.Collections.Generic;

namespace Meridian.Models.DTOs
{
    public class UserDto
    {
        public UserDto()
        {
            RoleDtos = new HashSet<RoleDto>();
            CompanyDtos = new HashSet<CompanyDto>();
        }

        public Guid ID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PIN { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LastActive { get; set; }
        public ICollection<RoleDto> RoleDtos{ get; set; }
        public ICollection<CompanyDto> CompanyDtos { get; set; }
    }
}
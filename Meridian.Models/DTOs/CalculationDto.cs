﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CalculationDto
    {
        public CalculationDto()
        {
            CalculationItemDtos = new HashSet<CalculationItemDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        public Guid? CookingID { get; set; }

        [MaxLength(1024)]
        public string Comment { get; set; }

        public int Quantity { get; set; }
        public double? Summ { get; set; }
        public double? Weight { get; set; }
        public CookingDto Cooking { get; set; }
        public ICollection<CalculationItemDto> CalculationItemDtos { get; set; }
    }
}
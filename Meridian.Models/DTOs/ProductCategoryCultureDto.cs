﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class ProductCategoryCultureDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public Guid CultureID { get; set; }
        public Guid ProductCategoryID { get; set; }
        public ProductCategoryDto ProductCategoryDto { get; set; }
        public CultureDto CultureDto { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class UnitCultureDto
    {
        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        public Guid CultureID { get; set; }
        public Guid UnitID { get; set; }
        public CultureDto CultureDto { get; set; }
        public UnitDto UnitDto { get; set; }
    }
}
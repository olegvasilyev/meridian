﻿using Meridian.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class CookingDto
    {
        public CookingDto()
        {
            CalculationDtos = new HashSet<CalculationDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(32)]
        public string ModifiedBy { get; set; }

        [MaxLength(256)]
        public string Comment { get; set; }

        public Guid ProductID { get; set; }
        public Guid KitchenID { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public OrderPriority OrderPriority { get; set; }
        public ProductDto Product { get; set; }
        public KitchenDto Kitchen { get; set; }
        public ICollection<CalculationDto> CalculationDtos { get; set; }
    }
}
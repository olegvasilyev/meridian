﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class KitchenDto
    {
        public KitchenDto()
        {
            CookingDtos = new HashSet<CookingDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public Guid CompanyID { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public CompanyDto Company { get; set; }
        public ICollection<CookingDto> CookingDtos { get; set; }
    }
}
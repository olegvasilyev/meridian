﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Models.DTOs
{
    public class UnitDto
    {
        public UnitDto()
        {
            UnitCultureDtos = new HashSet<UnitCultureDto>();
            ProductDtos = new HashSet<ProductDto>();
        }

        public Guid ID { get; set; }
        public DateTime Created { get; set; }

        [Required]
        [MaxLength(32)]
        public string CreatedBy { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        public ICollection<UnitCultureDto> UnitCultureDtos { get; set; }
        public ICollection<ProductDto> ProductDtos { get; set; }
    }
}
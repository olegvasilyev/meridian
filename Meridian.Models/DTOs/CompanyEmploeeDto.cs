﻿using System;

namespace Meridian.Models.DTOs
{
    public class CompanyEmploeeDto
    {
        public Guid UserID { get; set; }
        public Guid CompanyID { get; set; }
        public CompanyDto CompanyDto { get; set; }
        public UserDto UserDto { get; set; }
    }
}
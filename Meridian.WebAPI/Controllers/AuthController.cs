﻿using AutoMapper;
using Meridian.Models.DTOs;
using Meridian.Models.Entities;
using Meridian.Models.Helpers;
using Meridian.Models.Models;
using Meridian.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Meridian.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly UserService _userService;
        private readonly SettingService _settingService;

        public AuthController(IConfiguration config, IMapper mapper, UserManager<User> userManager, UserService usersService, SettingService settingsService)
        {
            _config = config;
            _mapper = mapper;
            _userManager = userManager;
            _userService = usersService;
            _settingService = settingsService;
        }

        [HttpGet("userName")]
        public async Task<IActionResult> GetUserByUserName(string userName)
        {
            var userDto = await _userService.GetUserByUserNameAsync(userName);
            return Ok(userDto);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginModel userLoginModel)
        {
            if (!ModelState.IsValid)
                return BadRequest("Error login data");

            var user = await _userService.GetUserAsync(userLoginModel.UserName);

            if (user == null)
                return NotFound("No such user in the system");

            var result = await _userManager.CheckPasswordAsync(user, userLoginModel.Password);

            if (result)
            {
                var culture = await _settingService.GetDefaultCultureAsync();
                var currency = await _settingService.GetDefultCurrencyAsync();
                var appUser = _mapper.Map<UserDto>(user);
                appUser.RoleDtos = await _userService.GetRolesForUserAsync(user.Id);

                return Ok(new LoginResultModel
                {
                    Token = GenerateJwtToken(user).Result,
                    User = appUser,
                    Culture = culture,
                    Currency = currency
                });
            }

            return Unauthorized("Error login data");
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterUserDto registerUserDto)
        {
            if (!ModelState.IsValid)
                return BadRequest("Error register new user date");

            var existingUser = await _userManager.FindByNameAsync(registerUserDto.UserName);

            if (existingUser != null)
                return BadRequest("A user with this login already exists");

            var userToCreate = _mapper.Map<User>(registerUserDto);

            var result = await _userManager.CreateAsync(userToCreate, registerUserDto.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(userToCreate, Constants.CUSTOMER);
                var userToReturn = _mapper.Map<UserDto>(userToCreate);
                //return CreatedAtRoute("GetUser", new { Controller = "Users", id = userToCreate.Id }, userToReturn);
                return Ok(userToReturn);
            }

            return BadRequest(result.Errors);
        }

        private async Task<string> GenerateJwtToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
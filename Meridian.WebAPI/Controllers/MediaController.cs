﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;

namespace Meridian.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private readonly IWebHostEnvironment _appEnvironment;

        public MediaController(IWebHostEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile uploadedFile)
        {
            try
            {
                if (uploadedFile != null)
                {
                    var folder = Path.Combine(_appEnvironment.ContentRootPath, "Files");

                    if (!Directory.Exists(folder))
                        Directory.CreateDirectory(folder);
                    var filePath = Path.Combine(folder, uploadedFile.FileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await uploadedFile.CopyToAsync(fileStream);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _ = ex.Message;
            }

            return Ok();
        }
    }
}
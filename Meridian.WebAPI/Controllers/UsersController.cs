﻿using Meridian.Data.Context;
using Meridian.Services.Services;
using Meridian.WebAPI.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Meridian.WebAPI.Controllers
{
    //[Authorize(Roles = "Administrator")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService usersService)
        {
            _userService = usersService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            //var ctx = HttpContext.GetContext();
            var userDtos = await _userService.GetUsersAsync();

            return Ok(userDtos);
        }
    }
}
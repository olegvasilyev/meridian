﻿using Meridian.Models.DTOs;
using Meridian.Services.Services;
using Meridian.WebAPI.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Meridian.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductService _productsService;

        public ProductsController(ProductService productsService)
        {
            _productsService = productsService;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateProduct(CreateProductDto createProductDto)
        {
            if (!ModelState.IsValid)
                return BadRequest("Error request data");

            var ctx = HttpContext.GetContext();

            var result = await _productsService.CreateProduct(ctx, createProductDto);

            return Ok(result);
        }

        //[HttpGet]
        //public async Task<IActionResult> GetProducts()
        //{
        //    var result = await _productsService.GetProducts();

        //    return Ok(result);
        //}
    }
}
﻿using Meridian.Models.Helpers;
using Meridian.Models.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Globalization;

namespace Meridian.WebAPI.Helpers
{
    public static class HttpContextHelper
    {
        public static AuthenticationContext GetContext(this HttpContext context)
        {
            var userName = context.Request.Headers[nameof(AuthenticationContext.UserName)];

            if (string.IsNullOrEmpty(userName))
                return null;

            AuthenticationContext ctx = new AuthenticationContext
            {
                UserName = userName,
                TimeZoneID = (string)context.Request.Headers[nameof(AuthenticationContext.TimeZoneID)] ?? TimeZoneInfo.Local.Id,
                Culture = (string)context.Request.Headers[nameof(AuthenticationContext.Culture)] ?? CultureInfo.CurrentCulture.Name,
                Currency = (string)context.Request.Headers[nameof(AuthenticationContext.Currency)] ?? Constants.DEFAULTCURRENCY,
            };

            return ctx;
        }
    }
}
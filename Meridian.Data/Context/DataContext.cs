﻿using Meridian.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace Meridian.Data.Context
{
    public partial class DataContext : IdentityDbContext<User, Role, Guid, IdentityUserClaim<Guid>,
        UserRole, IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public virtual DbSet<Calculation> Calculations { get; set; }
        public virtual DbSet<CalculationItem> CalculationItems { get; set; }
        public virtual DbSet<Cashbox> Cashboxes { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyEmployee> CompanyEmployees { get; set; }
        public virtual DbSet<Contragent> Contragents { get; set; }
        public virtual DbSet<Cooking> Cookings { get; set; }
        public virtual DbSet<Culture> Cultures { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<CurrensyConvert> CurrencyConverts { get; set; }
        public virtual DbSet<Place> Places { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<Kitchen> Kitchens { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }
        public virtual DbSet<PackingItem> PackingItems { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductCategoryCulture> ProductCategoryCultures { get; set; }
        public virtual DbSet<ProductCurrency> ProductCurrencies { get; set; }
        public virtual DbSet<ProductCulture> ProductCultures { get; set; }
        public virtual DbSet<ProductPlace> ProductPlaces { get; set; }
        public virtual DbSet<ProductWarehouse> ProductWarehouses { get; set; }
        public virtual DbSet<SystemSetting> SystemSettings { get; set; }
        public virtual DbSet<WarehouseItem> WarehouseItems { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<WarehouseInvoice> WarehouseInvoices { get; set; }
        public virtual DbSet<Table> Tables { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<UnitCulture> UnitCultures { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=MEDIASTATION;Initial Catalog=Meridian;User ID=meridian;Password=123;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Calculation>().HasOne(x => x.Cooking).WithMany(x => x.Calculations).HasForeignKey(x => x.CookingID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<CalculationItem>().HasOne(x => x.Product).WithMany(x => x.CalculationItems).HasForeignKey(x => x.ProductID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<CalculationItem>().HasOne(x => x.Warehouse).WithMany(x => x.CalculationItems).HasForeignKey(x => x.WarehouseID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Cashbox>().HasOne(x => x.Company).WithMany(x => x.Cashboxes).HasForeignKey(x => x.CompanyID).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Cashbox>().HasOne(x => x.Currency).WithMany(x => x.Cashboxes).HasForeignKey(x => x.CurrencyID).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Cashbox>().HasOne(x => x.Place).WithMany(x => x.Cashboxes).HasForeignKey(x => x.PlaceID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Company>().HasOne(x => x.Image).WithMany(x => x.Companies).HasForeignKey(x => x.ImageID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<CompanyEmployee>().HasOne(x => x.User).WithMany(x => x.CompanyEmployees).HasForeignKey(x => x.UserID).IsRequired();
            modelBuilder.Entity<CompanyEmployee>().HasOne(x => x.Company).WithMany(x => x.CompanyEmployees).HasForeignKey(x => x.CompanyID).IsRequired();
            modelBuilder.Entity<CompanyEmployee>().HasKey(x => new { x.CompanyID, x.UserID });
            modelBuilder.Entity<Culture>().HasOne(x => x.Image).WithMany(x => x.Cultures).HasForeignKey(x => x.ImageID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Culture>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<CurrensyConvert>().HasOne(x => x.SrcCurrency).WithMany(x => x.SrcCurrencies).HasForeignKey(x => x.SrcID).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<CurrensyConvert>().HasOne(x => x.DestCurrency).WithMany(x => x.DestCurrencies).HasForeignKey(x => x.DestID).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Invoice>().HasOne(x => x.Contragent).WithMany(x => x.Invoices).HasForeignKey(x => x.ContragentID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Invoice>().HasOne(x => x.Cashbox).WithMany(x => x.Invoices).HasForeignKey(x => x.CashboxID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Order>().HasOne(x => x.Table).WithMany(x => x.Orders).HasForeignKey(x => x.TableID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<PackingItem>().HasOne(x => x.Product).WithMany(x => x.PackingItems).HasForeignKey(x => x.ProductID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Product>().HasOne(x => x.Unit).WithMany(x => x.Products).HasForeignKey(x => x.UnitID).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Product>().HasOne(x => x.ProductCategory).WithMany(x => x.Products).HasForeignKey(x => x.ProductCategoryID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Product>().HasOne(x => x.Image).WithMany(x => x.Products).HasForeignKey(x => x.ImageID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<WarehouseInvoice>().HasOne(x => x.BasedOnInvoice).WithMany(x => x.WarehouseInvoices).HasForeignKey(x => x.BasedOnInvoiceID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<WarehouseItem>().HasOne(x => x.Product).WithMany(x => x.WarehouseItems).HasForeignKey(x => x.ProductID).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Place>().HasOne(x => x.Company).WithMany(x => x.Places).HasForeignKey(x => x.CompanyID).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Warehouse>().HasOne(x => x.Company).WithMany(x => x.Warehouses).HasForeignKey(x => x.CompanyID).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<UserRole>().HasOne(x => x.Role).WithMany(x => x.UserRoles).HasForeignKey(x => x.RoleId).IsRequired();
            modelBuilder.Entity<UserRole>().HasOne(x => x.User).WithMany(x => x.UserRoles).HasForeignKey(x => x.UserId).IsRequired();
        }
    }
}
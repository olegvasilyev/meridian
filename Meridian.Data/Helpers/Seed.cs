﻿using Meridian.Data.Context;
using Meridian.Models.Entities;
using Meridian.Models.Helpers;
using Microsoft.AspNetCore.Identity;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Meridian.Data.Helpers
{
    public static class Seed
    {
        public static async Task FillDatabaseAsync(DataContext context, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            string userName = "system";
            DateTime dateTime = DateTime.UtcNow;

            var cultuteInfos = CultureInfo.GetCultures(CultureTypes.AllCultures).ToList();

            string[] names = { "uk-UA", "ru-RU", "en-US" };

            var selectedCultures = cultuteInfos.Where(x => names.Contains(x.Name)).ToList();

            foreach (var item in selectedCultures)
            {
                context.Cultures.Add(
                    new Culture
                    {
                        ID = Guid.NewGuid(),
                        Created = dateTime,
                        CreatedBy = userName,
                        Name = item.Name,
                        DisplayName = item.DisplayName
                    });
            }

            var company = new Company
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "Meridian Cafe",
                Contacts = "Phone: +3(095)123-45-67, Email: pr@meridian.com",
                Description = "Tasty, comfortable, positive!"
            };

            context.Companies.Add(company);

            var currencyUA = new Currency
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                EnglishName = "UAH",
                NativeName = "ГРН",
                CurrencySymbol = '₴'
            };

            var currencyUS = new Currency
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                EnglishName = "USD",
                NativeName = "USD",
                CurrencySymbol = '$'
            };

            var currencyEU = new Currency
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                EnglishName = "EUR",
                NativeName = "EUR",
                CurrencySymbol = '€'
            };

            var currencyRU = new Currency
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                EnglishName = "RUB",
                NativeName = "РУБ",
                CurrencySymbol = '₽'
            };

            context.Currencies.AddRange(currencyUS, currencyUA, currencyEU, currencyRU);

            var place = new Place
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "Летняя площадка",
                CompanyID = company.ID
            };

            context.Places.Add(place);

            for (int i = 1; i < 6; i++)
            {
                context.Tables.Add(
                    new Table
                    {
                        ID = Guid.NewGuid(),
                        Created = dateTime,
                        CreatedBy = userName,
                        Name = i.ToString(),
                        NumberOfSeats = 4,
                        PlaceID = place.ID
                    });
            }

            context.Cashboxes.Add(
                new Cashbox
                {
                    ID = Guid.NewGuid(),
                    Created = dateTime,
                    CreatedBy = userName,
                    Name = "Основная касса",
                    CurrencyID = currencyUA.ID,
                    CompanyID = company.ID,
                    PlaceID = place.ID
                });

            context.Warehouses.Add(
                new Warehouse
                {
                    ID = Guid.NewGuid(),
                    Created = dateTime,
                    CreatedBy = userName,
                    Name = "Основной склад",
                    CompanyID = company.ID,
                    Description = "Основной склад"
                });

            context.Kitchens.AddRange(
                new Kitchen
                {
                    ID = Guid.NewGuid(),
                    Created = dateTime,
                    CreatedBy = userName,
                    Name = "Основная кухня",
                    CompanyID = company.ID,
                },
                new Kitchen
                {
                    ID = Guid.NewGuid(),
                    Created = dateTime,
                    CreatedBy = userName,
                    Name = "Мангал",
                    CompanyID = company.ID,
                });

            context.SystemSettings.Add(new SystemSetting
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Code = SettingTypes.DefaultCulture.ToString(),
                SettingType = SettingTypes.DefaultCulture,
                Value = "ru-RU",
                ValueType = typeof(string).ToString(),
                Description = "Default culture name"
            });

            context.SystemSettings.Add(new SystemSetting
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Code = SettingTypes.DefaultCurrency.ToString(),
                SettingType = SettingTypes.DefaultCurrency,
                Value = currencyUA.EnglishName,
                ValueType = typeof(string).ToString(),
                Description = "Default currency english name"
            });

            context.SaveChanges();

            var uaCultureID = context.Cultures.First(x => x.Name.Equals("uk-UA")).ID;
            var ruCultureID = context.Cultures.First(x => x.Name.Equals("ru-RU")).ID;
            var usCultureID = context.Cultures.First(x => x.Name.Equals("en-US")).ID;

            #region Units

            var kg = new Unit
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "кг."
            };

            kg.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "кг.",
                CultureID = uaCultureID,
                UnitID = kg.ID
            });

            kg.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "кг.",
                CultureID = ruCultureID,
                UnitID = kg.ID
            });

            kg.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "kg.",
                CultureID = usCultureID,
                UnitID = kg.ID
            });

            var g = new Unit
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "гр."
            };

            g.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "гр.",
                CultureID = uaCultureID,
                UnitID = g.ID
            });

            g.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "гр.",
                CultureID = ruCultureID,
                UnitID = g.ID
            });

            g.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "g",
                CultureID = usCultureID,
                UnitID = g.ID
            });

            var pc = new Unit
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "шт."
            };

            pc.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "шт.",
                CultureID = uaCultureID,
                UnitID = pc.ID
            });

            pc.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "шт.",
                CultureID = ruCultureID,
                UnitID = pc.ID
            });

            pc.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "PC.",
                CultureID = usCultureID,
                UnitID = pc.ID
            });

            var ml = new Unit
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "мл."
            };

            ml.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "мл.",
                CultureID = uaCultureID,
                UnitID = ml.ID
            });

            ml.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "мл.",
                CultureID = ruCultureID,
                UnitID = ml.ID
            });

            ml.UnitCultures.Add(new UnitCulture
            {
                ID = Guid.NewGuid(),
                Created = dateTime,
                CreatedBy = userName,
                Name = "ml",
                CultureID = usCultureID,
                UnitID = ml.ID
            });

            context.Units.AddRange(kg, g, pc, ml);

            #endregion Units

            await context.SaveChangesAsync();

            if (!userManager.Users.Any())
            {
                foreach (var item in Constants.ROLES)
                {
                    var role = new Role { Name = item };
                    await roleManager.CreateAsync(role);
                }

                var admin = new User
                {
                    UserName = "admin",
                    FirstName = "Meridian",
                    LastName = "Administrator",
                    Created = DateTime.UtcNow,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                await userManager.CreateAsync(admin, "admin2020");
                await userManager.AddToRoleAsync(admin, Constants.ADMINISTRATOR);

                var accountant = new User
                {
                    UserName = "account",
                    FirstName = "Accountant",
                    LastName = "Meridian",
                    Created = DateTime.UtcNow,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                await userManager.CreateAsync(accountant, "account2020");
                await userManager.AddToRoleAsync(accountant, Constants.ACCOUNTANT);
                await userManager.AddToRoleAsync(accountant, Constants.CASHIER);

                var waiter = new User
                {
                    UserName = "waiter",
                    FirstName = "Waiter",
                    LastName = "Meridian",
                    Created = DateTime.UtcNow,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                await userManager.CreateAsync(waiter, "waiter2020");
                await userManager.AddToRoleAsync(waiter, Constants.WAITER);

                var cook = new User
                {
                    UserName = "cook",
                    FirstName = "Cook",
                    LastName = "Meridian",
                    Created = DateTime.UtcNow,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                await userManager.CreateAsync(cook, "cook2020");
                await userManager.AddToRoleAsync(cook, Constants.COOK);
                await userManager.AddToRoleAsync(cook, Constants.WAITER);

                var casher = new User
                {
                    UserName = "casher",
                    FirstName = "Casher",
                    LastName = "Meridian",
                    Created = DateTime.UtcNow,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                await userManager.CreateAsync(casher, "casher2020");
                await userManager.AddToRoleAsync(casher, Constants.CASHIER);

                var customer = new User
                {
                    UserName = "client",
                    FirstName = "Customer",
                    LastName = "Meridian",
                    Created = DateTime.UtcNow,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                await userManager.CreateAsync(customer, "client2020");
                await userManager.AddToRoleAsync(customer, Constants.CUSTOMER);

                company.CompanyEmployees.Add(new CompanyEmployee { CompanyID = company.ID, UserID = admin.Id });
                company.CompanyEmployees.Add(new CompanyEmployee { CompanyID = company.ID, UserID = accountant.Id });
                company.CompanyEmployees.Add(new CompanyEmployee { CompanyID = company.ID, UserID = waiter.Id });
                company.CompanyEmployees.Add(new CompanyEmployee { CompanyID = company.ID, UserID = cook.Id });
                company.CompanyEmployees.Add(new CompanyEmployee { CompanyID = company.ID, UserID = casher.Id });

                await context.SaveChangesAsync();
            }

            SeedProducts.FillProducts(userName, context);

            await context.SaveChangesAsync();
        }
    }
}
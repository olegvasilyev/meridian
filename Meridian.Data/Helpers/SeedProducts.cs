﻿using Meridian.Data.Context;
using Meridian.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meridian.Data.Helpers
{
    public static class SeedProducts
    {
        private static readonly Random rand = new Random();

        public static void FillProducts(string userName, DataContext context)
        {
            var cultures = context.Cultures.ToList();

            var uaCultureID = context.Cultures.First(x => x.Name.Equals("uk-UA")).ID;
            var ruCultureID = context.Cultures.First(x => x.Name.Equals("ru-RU")).ID;
            var usCultureID = context.Cultures.First(x => x.Name.Equals("en-US")).ID;

            var mainCategory = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Основное меню",
                IsMenu = true,
                Order = 100,
                Markup = 1.5,
                MinimalAmount = 0
            };

            mainCategory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Основное меню",
                ProductCategoryID = mainCategory.ID,
                CultureID = ruCultureID
            });

            mainCategory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Основне меню",
                ProductCategoryID = mainCategory.ID,
                CultureID = uaCultureID
            });

            mainCategory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Main menu",
                ProductCategoryID = mainCategory.ID,
                CultureID = usCultureID
            });

            var bbqCategory = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Мангал меню",
                IsMenu = true,
                Order = 2,
                Markup = 2,
                MinimalAmount = 0
            };

            bbqCategory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Мангал меню",
                ProductCategoryID = bbqCategory.ID,
                CultureID = ruCultureID
            });

            bbqCategory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Мангал меню",
                ProductCategoryID = bbqCategory.ID,
                CultureID = uaCultureID
            });

            bbqCategory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "BBQ menu",
                ProductCategoryID = bbqCategory.ID,
                CultureID = usCultureID
            });

            var barCatrgory = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Бар",
                IsMenu = true,
                Order = 3,
                Markup = 1.5,
                MinimalAmount = 0
            };

            barCatrgory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Бар",
                ProductCategoryID = barCatrgory.ID,
                CultureID = ruCultureID
            });

            barCatrgory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Бар",
                ProductCategoryID = barCatrgory.ID,
                CultureID = uaCultureID
            });

            barCatrgory.ProductCategoryCultures.Add(new ProductCategoryCulture
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Bar",
                ProductCategoryID = barCatrgory.ID,
                CultureID = usCultureID
            });

            context.ProductCategories.AddRange(mainCategory, bbqCategory, barCatrgory);

            ProductCategory meat = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Мясо",
                IsMenu = false,
                Order = 1,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                meat.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{meat.Name}",
                    CultureID = item.ID,
                });
            }

            ProductCategory seafood = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Морепродукты",
                IsMenu = false,
                Order = 2,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                seafood.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{seafood.Name}",
                    CultureID = item.ID
                });
            }

            ProductCategory vegetables = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Овощи и фрукты",
                IsMenu = false,
                Order = 3,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                vegetables.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{vegetables.Name}",
                    CultureID = item.ID
                });
            }

            ProductCategory dairy = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Молочные продукты и яйца",
                IsMenu = false,
                Order = 4,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                dairy.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{dairy.Name}",
                    CultureID = item.ID
                });
            }

            ProductCategory nuts = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Сухофрукты и орехи",
                IsMenu = false,
                Order = 5,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                nuts.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{nuts.Name}",
                    CultureID = item.ID
                });
            }

            ProductCategory grocery = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Бакалея",
                IsMenu = false,
                Order = 6,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                grocery.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{grocery.Name}",
                    CultureID = item.ID
                });
            }

            ProductCategory cheeses = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Сыры",
                IsMenu = false,
                Order = 7,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                cheeses.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{cheeses.Name}",
                    CultureID = item.ID
                });
            }

            ProductCategory bakery = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Хлебобулочные изделия",
                IsMenu = false,
                Order = 8,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                bakery.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{bakery.Name}",
                    CultureID = item.ID
                });
            }

            ProductCategory spices = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Соусы и специи",
                IsMenu = false,
                Order = 9,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                spices.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{spices.Name}",
                    CultureID = item.ID
                });
            }

            ProductCategory beverages = new ProductCategory
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = "Напитки",
                IsMenu = true,
                Order = 10,
                Markup = 1.5,
                MinimalAmount = 1000,
            };

            foreach (var item in cultures)
            {
                beverages.ProductCategoryCultures.Add(new ProductCategoryCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{beverages.Name}",
                    CultureID = item.ID
                });
            }

            context.ProductCategories.AddRange(meat, seafood, vegetables, dairy, nuts, grocery, cheeses, bakery, spices, beverages);

            var kg = context.Units.First(x => x.Name == "кг.");
            var g = context.Units.First(x => x.Name == "гр.");
            var ml = context.Units.First(x => x.Name == "мл.");
            var pc = context.Units.First(x => x.Name == "шт.");

            var currecyName = context.SystemSettings.First(x => x.Code.Equals(SettingTypes.DefaultCurrency.ToString()));
            var defaultCurrency = context.Currencies.First(x => x.EnglishName.Equals(currecyName.Value));

            context.Products.Add(CreateProduct("Свинина", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Филе цыплят", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Бедро цыплят", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Говядина", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Лопатка говяжья", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Стейк T-bone", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Телятина", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Филе гусиное", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Филе утки", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Филе индейки", userName, meat, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Крыло индейки", userName, meat, kg, defaultCurrency, cultures));

            context.Products.Add(CreateProduct("Скумбрия с/м ~800г", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Карась очищенный и потрошенный с/м ~800г", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Креветки королевские без головы в панцире 16/20 ТМ Barrakuda 1кг", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Креветка варено-мороженая в панцире с головой 120+ 1кг", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Сайда свежемороженая филе ~0,9кг", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Креветка очищенная в/м 100/200 1кг", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Кальмар филе-кусок холодного копчения ТМ Flagman 250г", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Наггетсы рыбные в панировке из рисовых хлопьев VICI 500г", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Морской коктейль соленый фирменный ТМ Водный мир 180г", userName, seafood, kg, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Креветки в панцире с головой варено-мороженые 70/90 VICI 500г", userName, seafood, kg, defaultCurrency, cultures));

            context.Products.Add(CreateProduct("Яблоко Муцу ~1кг", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Яблоко Ред Чиф ~1кг", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Яблоко Голден ~1кг", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Банан ~1кг", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Абрикос", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Нектарин ~1кг", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Персик", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Голубика", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Киви ~1кг", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Лимон ~1кг", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Манго", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Ананас Бутеиль", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));
            context.Products.Add(CreateProduct("Мандарин ~1кг", userName, vegetables, kg, defaultCurrency, cultures, 40, 200));

            context.Products.Add(CreateProduct("Напиток миндальный ультрапастеризованный 2,2% Santal 1л", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Продукт ферментированный (йогурт) соевый с персиком Alpro 150г", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Напиток из лесных орехов Hazelnut Original Alpro 1л", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Кефир 1% ТМ Простоквашино 870г", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Сливки ультрапастеризованные 10% порционные ТМ Бурьонка 10г*10шт.", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Бифидойогурт питьевой 1,5% ТМ Активиа 580г", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Бифидойогурт питьевой “Банан-Киви” 1,5% ТМ Активиа 580г", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Творог “Творожна традиція” ТМ Президент 0,2% 350г", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Сметана 15% ТМ Президент 350г", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Творог “Творожна традиція” ТМ Президент 5% 350г", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Яйца куриные столовые первой категории С1 “Ясенсвіт” лоток 10шт.", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Яйца куриные столовые первой категории С1 “Селянські” лоток 10шт.", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Йогурт с наполнителем “Клубника” 3,2% жирности ТМ Дольче 280г", userName, dairy, pc, defaultCurrency, cultures));
            context.Products.Add(CreateProduct("Йогурт с наполнителем “Черника” 2,5% жирности ТМ Дольче 290г", userName, dairy, pc, defaultCurrency, cultures));

            context.Products.Add(CreateProduct("Кедровый орех 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Фундук жареный 250г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Фундук сырой 250г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Фисташка очищенная Аргентина 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Изюм синий 250г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Изюм 250г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Миндаль жареный 250г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Миндаль сырой 250г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Кешью сырой 250г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Арахис жареный 1кг", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Арахис сырой 1кг", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Манго ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Дыня «Канталуп» ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Мандарин ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Папайя ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Клубника ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Банановые чипсы ТМ WINWAY 70г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Курага ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Клюква ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Финики ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Кешью ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Миндаль ТМ WINWAY 100г", userName, nuts, g, defaultCurrency, cultures, 50, 180));
            context.Products.Add(CreateProduct("Фисташки ТМ WINWAY 80г", userName, nuts, g, defaultCurrency, cultures, 50, 180));

            context.Products.Add(CreateProduct("Макароны Girandole Torsades №4 BARILLA 500г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Подсолнечное масло рафинированное дезодорированное HOSFO FAMILY 15л", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Подсолнечное масло високоолеиновое рафинированное дезодорированное HOSFO CHEF 15л", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Масло подсолнечное (рафинированное) марки “П” ТМ Стожар 870мл", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Мука пшеничная высшего сорта ТМ Тростянецьке борошно 2кг", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Оливковое масло Pomace ТМ Olio Dante 5л", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Оливковое масло Pomace ТМ Olio Dante 1л", userName, grocery, pc, defaultCurrency, cultures, 80, 1200));
            context.Products.Add(CreateProduct("Оливковое масло Extra Virgin II Mediterraneo ТМ Olio Dante 1л", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Крем-суп Грибной ТМ Street Soup 50г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Сахар коричневый нерафинированный тростниковый DEMERARA порционный ТМ Саркара 500г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Гранола МорковьТМ Dr.Granola 125г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Гранола Тыква&Яблоко&КорицаТМ Dr.Granola 125г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Гранола The Original Granola ТМ Dr.Granola 150г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Крем-суп Томатный ТМ Street Soup 50г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Крем-суп Нутовый ТМ Street Soup 50г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Крем-суп Гороховый ТМ Street Soup 50г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Крем-суп из чечевицы ТМ Street Soup 50г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Крем-суп Гороховый с говядиной ТМ Street Soup 50г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Масло подсолнечное домашнее украинское Extra Virgin ТМ Golden Kings of Ukraine 500мл", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Масло кунжутное Extra Virgin ТМ Golden Kings of Ukraine 350мл", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Масло льняное Extra Virgin ТМ Golden Kings of Ukraine 350мл", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Масло тыквенное Extra Virgin ТМ Golden Kings of Ukraine 350мл", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Ореховая смесь “Лесная” ТМ WINWAY 100г", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Сахар белый кристаллический ТМ Фудси 10кг", userName, grocery, pc, defaultCurrency, cultures, 80, 150));
            context.Products.Add(CreateProduct("Масло подсолнечное (рафинированное) марки “П” ТМ Стожар 5л", userName, grocery, pc, defaultCurrency, cultures, 80, 150));

            context.Products.Add(CreateProduct("Сыр Грана Падано ~1кг", userName, cheeses, kg, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Крем-сыр Original 61% Philadelphia 175г", userName, cheeses, pc, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Крем-сыр с зеленью 59% Philadelphia 175г", userName, cheeses, pc, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Сыр плавленный “Дружба” 55% ТМ Комо 90г", userName, cheeses, pc, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Сыр “Моцарелла La Perla” тертая 1кг", userName, cheeses, kg, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Сыр Голландский 45% ТМ Пирятин ~500г", userName, cheeses, kg, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Сыр Голландский 45% брус ТМ Пирятин ~5кг", userName, cheeses, kg, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Сыр твердый “Сметанковый” квадрат 50% ТМ Высь ~2кг", userName, cheeses, kg, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Сыр Грана Падано 18 месяцев Original ~0,8кг", userName, cheeses, kg, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Сыр Грана Падано 12 месяцев Original ~0,8кг", userName, cheeses, kg, defaultCurrency, cultures, 400, 600));
            context.Products.Add(CreateProduct("Сыр Hard Cheese 12 месяцев 37% Leone ~0,8кг", userName, cheeses, kg, defaultCurrency, cultures, 100, 300));
            context.Products.Add(CreateProduct("Сыр Hard Cheese 12 месяцев 37% Leone ~30кг", userName, cheeses, kg, defaultCurrency, cultures, 100, 300));

            context.Products.Add(CreateProduct("Палочки хлебные “Гриссини” со смесью итальянских трав ТМ Скиба 200г", userName, bakery, pc, defaultCurrency, cultures, 10, 50));
            context.Products.Add(CreateProduct("Палочки хлебные “Гриссини” с кунжутом ТМ Скиба 200г", userName, bakery, pc, defaultCurrency, cultures, 10, 50));
            context.Products.Add(CreateProduct("Палочки хлебные “Гриссини” с луком и укропом ТМ Скиба 200г", userName, bakery, pc, defaultCurrency, cultures, 10, 50));
            context.Products.Add(CreateProduct("Палочки хлебные “Гриссини” с солью ТМ Скиба 200г", userName, bakery, pc, defaultCurrency, cultures, 10, 50));
            context.Products.Add(CreateProduct("Хлеб из муки грубого помола на закваске 500г", userName, bakery, pc, defaultCurrency, cultures, 10, 50));
            context.Products.Add(CreateProduct("Чиабатта классическая 90г (2шт.)", userName, bakery, pc, defaultCurrency, cultures, 10, 50));
            context.Products.Add(CreateProduct("Чиабатта классическая 60г (2шт.)", userName, bakery, pc, defaultCurrency, cultures, 10, 50));

            context.Products.Add(CreateProduct("Генуэзский соус с базиликом Pesto Genovese BARILLA 190г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Соус Pesto с базиликом и рукколой BARILLA 190г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Соус томатный с базиликом Basilico BARILLA 400г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Горчица “Казацкая” Торчин 130г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Заправка свекольно-томатная Торчин 240г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Горчица “Американская” Торчин 130г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Кетчуп “До шашлику” Торчин 270г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Кетчуп “Лагідний” Торчин 270г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Микс грибов сушеных ТМ Дари Гуцульщини 50г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Микс грибов сушеных ТМ Дари Гуцульщини 100г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Грибы белые сушеные молотые ТМ Дари Гуцульщини 100г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Грибы лисички сушеные молотые ТМ Дари Гуцульщини 100г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Горчица “Медовая” ТМ Чумак 120г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Соус “Техасский бабекю” ТМ ЧУМАК 200г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Кетчуп “Лагідний для дітей” ТМ ЧУМАК 200г", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Чеснок со сливками ТМ Scandia Pepparrot 180мл", userName, spices, pc, defaultCurrency, cultures, 70, 400));
            context.Products.Add(CreateProduct("Хрен васаби со сливками ТМ Scandia Pepparrot 190г", userName, spices, pc, defaultCurrency, cultures, 70, 400));

            context.Products.Add(CreateProduct("Апельсиновый сок ТМ Yan 930мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Гранатовый сок ТМ Yan 930мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Яблочный сок ТМ Yan 930мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Ананасовый сок ТМ Yan 930мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Сок Манго ТМ Yan 930мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Тропический сок ТМ Yan 930мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Нектар Клубнично-банановый ТМ SIS 1,6л", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Персиковый нектар ТМ SIS 1,6л", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Апельсиновый нектар ТМ SIS 1,6л", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Напиток Coca-Cola 500мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Напиток Coca-Cola 330мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Напиток Coca-Cola Light 330мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Напиток Coca-Cola Zero 500мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Напиток Sprite 1л", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
            context.Products.Add(CreateProduct("Напиток Sprite 500мл", userName, beverages, pc, defaultCurrency, cultures, 70, 150));
        }

        private static Product CreateProduct(string name, string userName, ProductCategory category, Unit unit, Currency currency, List<Culture> cultures, int min = 50, int max = 500)
        {
            var product = new Product
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                Name = name,
                IsMenu = false,
                IsProtected = true,
                MinimalAmount = 1,
                ProductType = ProductType.Product,
                ProductCategoryID = category.ID,
                UnitID = unit.ID,
            };

            foreach (var item in cultures)
            {
                var printerName = $"{item.Name}-{product.Name}";

                product.ProductCultures.Add(new ProductCulture
                {
                    ID = Guid.NewGuid(),
                    Created = DateTime.UtcNow,
                    CreatedBy = userName,
                    Name = $"{item.Name}-{product.Name}",
                    PrinterName = printerName.Length > 32 ? $"{printerName.Substring(0, 28)}..." : printerName,
                    CultureID = item.ID,
                    ProductID = product.ID
                });
            }

            var entry = rand.Next(min, max);
            var retail = rand.Next(entry, entry * 2);
            var markup = retail / entry;

            product.ProductCurrencies.Add(new ProductCurrency
            {
                ID = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CreatedBy = userName,
                CurrencyID = currency.ID,
                ProductID = product.ID,
                EntryPrice = entry,
                Markup = markup,
                RetailPrice = retail
            });

            return product;
        }
    }
}
﻿using System.Linq;

namespace Meridian.Data.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Get();

        void Create(TEntity item);

        void Update(TEntity item);

        void Delete(TEntity item);
    }
}
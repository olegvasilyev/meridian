﻿using Meridian.Data.Context;
using Meridian.Models.Entities;
using System;
using System.Threading.Tasks;

namespace Meridian.Data.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private IGenericRepository<Calculation> _calculationsRepository;
        private IGenericRepository<CalculationItem> _calculationItemsRepository;
        private IGenericRepository<Cashbox> _cashboxesRepository;
        private IGenericRepository<Company> _companiesRepository;
        private IGenericRepository<CompanyEmployee> _companyEmploeesRepository;
        private IGenericRepository<Contragent> _contragentsRepository;
        private IGenericRepository<Cooking> _cookingsRepository;
        private IGenericRepository<Culture> _culturesRepository;
        private IGenericRepository<Currency> _currenciesRepository;
        private IGenericRepository<Image> _imagesRepository;
        private IGenericRepository<Invoice> _invoicesRepository;
        private IGenericRepository<Kitchen> _kitchensRepository;
        private IGenericRepository<Order> _ordersRepository;
        private IGenericRepository<OrderItem> _orderItemsRepository;
        private IGenericRepository<PackingItem> _packingItemsRepository;
        private IGenericRepository<Place> _placeItemsRepository;
        private IGenericRepository<Product> _productsRepository;
        private IGenericRepository<ProductCategory> _productCategoriesRepository;
        private IGenericRepository<ProductCategoryCulture> _productCategoryCulturesRepository;
        private IGenericRepository<ProductCurrency> _productCurrenciesRepository;
        private IGenericRepository<ProductCulture> _productCulturesRepository;
        private IGenericRepository<ProductPlace> _productPlacesRepository;
        private IGenericRepository<ProductWarehouse> _productWarehousesRepository;
        private IGenericRepository<SystemSetting> _systemSettingsRepository;
        private IGenericRepository<Table> _tablesRepository;
        private IGenericRepository<Unit> _unitsRepository;
        private IGenericRepository<Role> _rolesRepository;
        private IGenericRepository<User> _usersRepository;
        private IGenericRepository<UserRole> _userRolesRepository;
        private IGenericRepository<UnitCulture> _unitCulturesRepository;
        private IGenericRepository<Warehouse> _warehousesRepository;
        private IGenericRepository<WarehouseInvoice> _warehouseInvoicesRepository;
        private IGenericRepository<WarehouseItem> _warehouseItemsRepository;
        private readonly DataContext _context;

        private bool disposed = false;

        public UnitOfWork(DataContext context)
        {
            _context = context;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public IGenericRepository<Calculation> CalculationsRepository
        {
            get
            {
                if (_calculationsRepository == null)
                    _calculationsRepository = new GenericRepository<Calculation>(_context);
                return _calculationsRepository;
            }
        }

        public IGenericRepository<CalculationItem> CalculationItemsRepository
        {
            get
            {
                if (_calculationItemsRepository == null)
                    _calculationItemsRepository = new GenericRepository<CalculationItem>(_context);
                return _calculationItemsRepository;
            }
        }

        public IGenericRepository<Cashbox> CashboxesRepository
        {
            get
            {
                if (_cashboxesRepository == null)
                    _cashboxesRepository = new GenericRepository<Cashbox>(_context);
                return _cashboxesRepository;
            }
        }

        public IGenericRepository<Company> CompaniesRepository
        {
            get
            {
                if (_companiesRepository == null)
                    _companiesRepository = new GenericRepository<Company>(_context);
                return _companiesRepository;
            }
        }

        public IGenericRepository<CompanyEmployee> CompanyEmployeesRepository
        {
            get
            {
                if (_companyEmploeesRepository == null)
                    _companyEmploeesRepository = new GenericRepository<CompanyEmployee>(_context);
                return _companyEmploeesRepository;
            }
        }

        public IGenericRepository<Contragent> ContragentsRepository
        {
            get
            {
                if (_contragentsRepository == null)
                    _contragentsRepository = new GenericRepository<Contragent>(_context);
                return _contragentsRepository;
            }
        }

        public IGenericRepository<Cooking> CookingsRepository
        {
            get
            {
                if (_cookingsRepository == null)
                    _cookingsRepository = new GenericRepository<Cooking>(_context);
                return _cookingsRepository;
            }
        }

        public IGenericRepository<Culture> CulturesRepository
        {
            get
            {
                if (_culturesRepository == null)
                    _culturesRepository = new GenericRepository<Culture>(_context);
                return _culturesRepository;
            }
        }

        public IGenericRepository<Currency> CurrenciesRepository
        {
            get
            {
                if (_currenciesRepository == null)
                    _currenciesRepository = new GenericRepository<Currency>(_context);
                return _currenciesRepository;
            }
        }

        public IGenericRepository<Image> ImagesRepository
        {
            get
            {
                if (_imagesRepository == null)
                    _imagesRepository = new GenericRepository<Image>(_context);
                return _imagesRepository;
            }
        }

        public IGenericRepository<Invoice> InvoicesRepository
        {
            get
            {
                if (_invoicesRepository == null)
                    _invoicesRepository = new GenericRepository<Invoice>(_context);
                return _invoicesRepository;
            }
        }

        public IGenericRepository<Kitchen> KitchensRepository
        {
            get
            {
                if (_kitchensRepository == null)
                    _kitchensRepository = new GenericRepository<Kitchen>(_context);
                return _kitchensRepository;
            }
        }

        public IGenericRepository<Order> OrdersRepository
        {
            get
            {
                if (_ordersRepository == null)
                    _ordersRepository = new GenericRepository<Order>(_context);
                return _ordersRepository;
            }
        }

        public IGenericRepository<OrderItem> OrderItemsRepository
        {
            get
            {
                if (_orderItemsRepository == null)
                    _orderItemsRepository = new GenericRepository<OrderItem>(_context);
                return _orderItemsRepository;
            }
        }

        public IGenericRepository<PackingItem> PackingItemsRepository
        {
            get
            {
                if (_packingItemsRepository == null)
                    _packingItemsRepository = new GenericRepository<PackingItem>(_context);
                return _packingItemsRepository;
            }
        }

        public IGenericRepository<Place> PlaceItemsRepository
        {
            get
            {
                if (_placeItemsRepository == null)
                    _placeItemsRepository = new GenericRepository<Place>(_context);
                return _placeItemsRepository;
            }
        }

        public IGenericRepository<Product> ProductsRepository
        {
            get
            {
                if (_productsRepository == null)
                    _productsRepository = new GenericRepository<Product>(_context);
                return _productsRepository;
            }
        }

        public IGenericRepository<ProductCategory> ProductCategoriesRepository
        {
            get
            {
                if (_productCategoriesRepository == null)
                    _productCategoriesRepository = new GenericRepository<ProductCategory>(_context);
                return _productCategoriesRepository;
            }
        }

        public IGenericRepository<ProductCategoryCulture> ProductCategoryCulturesRepository
        {
            get
            {
                if (_productCategoryCulturesRepository == null)
                    _productCategoryCulturesRepository = new GenericRepository<ProductCategoryCulture>(_context);
                return _productCategoryCulturesRepository;
            }
        }

        public IGenericRepository<ProductCurrency> ProductCurrenciesRepository
        {
            get
            {
                if (_productCurrenciesRepository == null)
                    _productCurrenciesRepository = new GenericRepository<ProductCurrency>(_context);
                return _productCurrenciesRepository;
            }
        }

        public IGenericRepository<ProductCulture> ProductCulturesRepository
        {
            get
            {
                if (_productCulturesRepository == null)
                    _productCulturesRepository = new GenericRepository<ProductCulture>(_context);
                return _productCulturesRepository;
            }
        }

        public IGenericRepository<ProductPlace> ProductPlacesRepository
        {
            get
            {
                if (_productPlacesRepository == null)
                    _productPlacesRepository = new GenericRepository<ProductPlace>(_context);
                return _productPlacesRepository;
            }
        }

        public IGenericRepository<ProductWarehouse> ProductWarehousesRepository
        {
            get
            {
                if (_productWarehousesRepository == null)
                    _productWarehousesRepository = new GenericRepository<ProductWarehouse>(_context);
                return _productWarehousesRepository;
            }
        }

        public IGenericRepository<SystemSetting> SystemSettingsRepository
        {
            get
            {
                if (_systemSettingsRepository == null)
                    _systemSettingsRepository = new GenericRepository<SystemSetting>(_context);
                return _systemSettingsRepository;
            }
        }

        public IGenericRepository<Table> TablesRepository
        {
            get
            {
                if (_tablesRepository == null)
                    _tablesRepository = new GenericRepository<Table>(_context);
                return _tablesRepository;
            }
        }

        public IGenericRepository<Role> RolesRepository
        {
            get
            {
                if (_rolesRepository == null)
                    _rolesRepository = new GenericRepository<Role>(_context);
                return _rolesRepository;
            }
        }

        public IGenericRepository<Unit> UnitsRepository
        {
            get
            {
                if (_unitsRepository == null)
                    _unitsRepository = new GenericRepository<Unit>(_context);
                return _unitsRepository;
            }
        }

        public IGenericRepository<User> UsersRepository
        {
            get
            {
                if (_usersRepository == null)
                    _usersRepository = new GenericRepository<User>(_context);
                return _usersRepository;
            }
        }

        public IGenericRepository<UserRole> UserRolesRepository
        {
            get
            {
                if (_userRolesRepository == null)
                    _userRolesRepository = new GenericRepository<UserRole>(_context);
                return _userRolesRepository;
            }
        }

        public IGenericRepository<UnitCulture> UnitCulturesRepository
        {
            get
            {
                if (_unitCulturesRepository == null)
                    _unitCulturesRepository = new GenericRepository<UnitCulture>(_context);
                return _unitCulturesRepository;
            }
        }

        public IGenericRepository<Warehouse> WarehousesRepository
        {
            get
            {
                if (_warehousesRepository == null)
                    _warehousesRepository = new GenericRepository<Warehouse>(_context);
                return _warehousesRepository;
            }
        }

        public IGenericRepository<WarehouseInvoice> WarehouseInvoicesRepository
        {
            get
            {
                if (_warehouseInvoicesRepository == null)
                    _warehouseInvoicesRepository = new GenericRepository<WarehouseInvoice>(_context);
                return _warehouseInvoicesRepository;
            }
        }

        public IGenericRepository<WarehouseItem> WarehouseItemsRepository
        {
            get
            {
                if (_warehouseItemsRepository == null)
                    _warehouseItemsRepository = new GenericRepository<WarehouseItem>(_context);
                return _warehouseItemsRepository;
            }
        }
    }
}
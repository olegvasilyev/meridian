﻿using Meridian.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Meridian.Data.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly DataContext _context;

        public GenericRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(TEntity item)
        {
            _context.Add<TEntity>(item);
        }

        public void Delete(TEntity item)
        {
            _context.Remove<TEntity>(item);
        }

        public IQueryable<TEntity> Get()
        {
            return _context.Set<TEntity>();
        }

        public void Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
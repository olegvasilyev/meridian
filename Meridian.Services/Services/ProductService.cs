﻿using AutoMapper;
using Meridian.Data.Context;
using Meridian.Models.Entities;
using Meridian.Data.Repositories;
using Meridian.Models.DTOs;
using Meridian.Models.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Meridian.Services.Services
{
    public class ProductService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly DataContext _context;

        public ProductService(UnitOfWork unitOfWork, IMapper mapper, DataContext context)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _context = context;
        }

        public async Task<ProductDto> CreateProduct(AuthenticationContext ctx, CreateProductDto createProductDto)
        {
            var product = _mapper.Map<CreateProductDto, Product>(createProductDto);

            product.CreatedBy = ctx.UserName;

            foreach (var item in product.ProductCultures)
            {
                item.CreatedBy = ctx.UserName;
            }

            foreach (var item in product.ProductWarehouses)
            {
                item.CreatedBy = ctx.UserName;
            }

            _unitOfWork.ProductsRepository.Create(product);

            await _unitOfWork.SaveChangesAsync();

            var result = _mapper.Map<ProductDto>(product);

            return result;
        }

        public async Task<List<ProductDto>> GetProducts()
        {
            List<ProductDto> result = new List<ProductDto>();

            var products = await _unitOfWork.ProductsRepository.Get()
                .Include(x => x.ProductCurrencies)
                .Include(x => x.ProductCultures)
                .ToListAsync();

            result = _mapper.Map<List<ProductDto>>(products);

            return result;
        }

        public async Task<List<Product>> GetProducts2()
        {
            var products = await _context.Products.Include(x => x.ProductCurrencies).Include(x => x.ProductCultures).ToListAsync();

            var productDtos = _mapper.Map<List<Product>, List<ProductDto>>(products);

            return products;
        }
    }
}
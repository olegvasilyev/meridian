﻿using Meridian.Data.Context;
using Meridian.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Meridian.Services.Services
{
    public class CultureService
    {
        private readonly DataContext _context;

        public CultureService(DataContext context)
        {
            _context = context;
        }

        public async Task<Culture> GetCultureAsync(string name)
            => await _context.Cultures.FirstOrDefaultAsync(x => x.Name.Equals(name));
    }
}
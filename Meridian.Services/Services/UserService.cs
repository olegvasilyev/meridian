﻿using AutoMapper;
using Meridian.Data.Repositories;
using Meridian.Models.DTOs;
using Meridian.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meridian.Services.Services
{
    public class UserService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserService(UnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<RoleDto>> GetRolesForUserAsync(Guid userID)
        {
            var roles = await _unitOfWork.UserRolesRepository.Get()
                .Include(x => x.Role).Where(x => x.UserId == userID)
                .Select(x => x.Role).ToListAsync();

            var rolesDto = _mapper.Map<List<RoleDto>>(roles);

            return rolesDto;
        }

        public async Task<User> GetUserAsync(string userName) => await _unitOfWork.UsersRepository.Get().FirstOrDefaultAsync(x => x.UserName.Equals(userName));

        public async Task<UserDto> GetUserByUserNameAsync(string userName)
        {
            UserDto userDto = new UserDto();

            var user = await _unitOfWork.UsersRepository.Get()
                .Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .Include(x => x.CompanyEmployees)
                .ThenInclude(x => x.Company)
                .FirstOrDefaultAsync(x => x.UserName.Equals(userName));

            userDto = _mapper.Map<UserDto>(user);

            foreach (var item in user.CompanyEmployees)
            {
                userDto.CompanyDtos.Add(_mapper.Map<CompanyDto>(item.Company));
            }

            foreach (var item in user.UserRoles)
            {
                userDto.RoleDtos.Add(_mapper.Map<RoleDto>(item.Role));
            }

            return userDto;
        }

        public async Task<List<UserDto>> GetUsersAsync()
        {
            var userDtos = new List<UserDto>();

            var users = await _unitOfWork.UsersRepository.Get()
                .Include(x => x.UserRoles)
                .ThenInclude(ur => ur.Role)
                .ToListAsync();

            userDtos = _mapper.Map<List<UserDto>>(users);

            foreach (var userDto in userDtos)
            {
                var user = users.First(x => x.Id.Equals(userDto.ID));

                foreach (var userRole in user.UserRoles)
                {
                    var roleDto = _mapper.Map<RoleDto>(userRole.Role);
                    roleDto.UserRoleDtos = null;
                    userDto.RoleDtos.Add(roleDto);
                }
            }

            return userDtos;
        }

        public async Task<User> GetUserByUserName(string userName) => await _unitOfWork.UsersRepository.Get()
       .Include(x => x.CompanyEmployees).ThenInclude(x => x.Company)
       .Include(x => x.UserRoles).ThenInclude(x => x.Role)
       .FirstOrDefaultAsync(x => x.UserName.Equals(userName));
    }
}
﻿using Meridian.Data.Context;
using Meridian.Models.Entities;
using Meridian.Models.Helpers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Meridian.Services.Services
{
    public class SettingService
    {
        private readonly DataContext _context;

        public SettingService(DataContext context)
        {
            _context = context;
        }

        public async Task<string> GetDefaultCultureAsync()
        {
            var culture = await _context.SystemSettings.FirstOrDefaultAsync(x => x.SettingType.Equals(SettingTypes.DefaultCulture));

            return culture != null ? culture.Value : CultureInfo.CurrentCulture.Name;
        }

        public async Task<string> GetDefultCurrencyAsync()
        {
            var setting = await _context.SystemSettings
                .FirstOrDefaultAsync(x => x.SettingType.Equals(SettingTypes.DefaultCurrency));

            return setting != null ? setting.Value : Constants.DEFAULTCURRENCY;
        }

        public async Task<List<SystemSetting>> GetSystemDefaultsAsync()
        {
            var settingKeys = new[] { SettingTypes.DefaultCurrency, SettingTypes.DefaultCulture };

            return await _context.SystemSettings
                .Where(x => settingKeys.Contains(x.SettingType)).ToListAsync();
        }
    }
}
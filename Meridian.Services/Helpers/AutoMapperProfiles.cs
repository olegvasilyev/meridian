﻿using AutoMapper;
using Meridian.Models.Entities;
using Meridian.Models.DTOs;
using System;

namespace Meridian.Services.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Calculation, CalculationDto>();
            CreateMap<CalculationItem, CalculationItemDto>();
            CreateMap<Cashbox, CashboxDto>();
            CreateMap<Company, CompanyDto>();
            CreateMap<CompanyEmployee, CompanyEmploeeDto>();
            CreateMap<Contragent, ContragentDto>();
            CreateMap<Cooking, CookingDto>();
            CreateMap<Culture, CultureDto>();
            CreateMap<Currency, CurrencyConvertDto>();
            CreateMap<Currency, CurrencyDto>();
            CreateMap<Image, ImageDto>();
            CreateMap<Invoice, InvoiceDto>();
            CreateMap<Kitchen, KitchenDto>();
            CreateMap<Order, OrderDto>();
            CreateMap<OrderItem, OrderItemDto>();
            CreateMap<PackingItem, PackingItemDto>();
            CreateMap<Place, PlaceDto>();
            CreateMap<ProductCategoryCulture, ProductCategoryCultureDto>();
            CreateMap<ProductCategory, ProductCategoryDto>();
            CreateMap<ProductCurrency, ProductCurrencyDto>();
            CreateMap<ProductCulture, ProductCultureDto>();
            CreateMap<ProductPlace, ProductPlaceDto>();
            CreateMap<ProductWarehouse, ProductWarehouseDto>();
            CreateMap<Role, RoleDto>();
            CreateMap<Table, TableDto>();
            CreateMap<UnitCulture, UnitCultureDto>();
            CreateMap<Unit, UnitDto>();
            CreateMap<UserRole, UserRoleDto>();
            CreateMap<User, UserDto>();
            CreateMap<Warehouse, WarehouseDto>();
            CreateMap<WarehouseInvoice, WarehouseInvoiceDto>();
            CreateMap<WarehouseItem, WarehouseItemDto>();
            CreateMap<Product, ProductDto>();

            CreateMap<CreateProductCultureDto, ProductCulture>();
            CreateMap<CreateProductWarehouseDto, ProductWarehouse>();
            CreateMap<CreateProductDto, Product>()
                .ForMember(dest => dest.ProductCultures, opt => opt.MapFrom(src => src.CreateProductCultureDtos))
                .ForMember(dest => dest.ProductWarehouses, opt => opt.MapFrom(src => src.CreateProductWarehouseDtos))
                .AfterMap((src, dest) =>
                {
                    var created = DateTime.UtcNow;

                    dest.ID = Guid.NewGuid();
                    dest.Created = created;

                    foreach (var item in dest.ProductCultures)
                    {
                        item.ID = Guid.NewGuid();
                        item.Created = created;
                        item.ProductID = dest.ID;
                    }

                    foreach (var item in dest.ProductWarehouses)
                    {
                        item.ID = Guid.NewGuid();
                        item.Created = created;
                        item.ProductID = dest.ID;
                    }
                });

            CreateMap<CreateUserDto, User>();
            CreateMap<RegisterUserDto, User>()
                .AfterMap((src, dest) => { dest.Created = DateTime.UtcNow; });
            CreateMap<User, UserDto>();
        }
    }
}
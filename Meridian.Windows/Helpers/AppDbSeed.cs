﻿using Meridian.Windows.Data;
using Meridian.Windows.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Meridian.Windows.Helpers
{
    public class AppDbSeed
    {
        public static async Task SeedDataAsync(AppDbContext context)
        {
            if (!context.Settings.Any())
            {
                context.Settings.Add(new Setting
                {
                    Code = SettingCode.ApiEndPoint.ToString(),
                    SettingCode = SettingCode.ApiEndPoint,
                    Modified = DateTime.Now.Ticks,
                    Value = "http://localhost:52299/",
                    ValueType = typeof(string).ToString(),
                    Description = "Meridian API endpoint"
                });

                context.Settings.Add(new Setting
                {
                    Code = SettingCode.UICulture.ToString(),
                    SettingCode = SettingCode.UICulture,
                    Modified = DateTime.Now.Ticks,
                    Value = "ru-RU",
                    ValueType = typeof(string).ToString(),
                    Description = "UI culture"
                });

                await context.SaveChangesAsync();
            }
        }
    }
}
﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Meridian.Windows.Models;
using Meridian.Windows.Services;
using Microsoft.Extensions.DependencyInjection;
using T = Telerik.Windows.Controls;

namespace Meridian.Windows.ViewModels
{
    public class AdminViewModel : ViewModelBase
    {
        private readonly AuthService _authService;

        public RelayCommand<object> LogoutCommand { get; set; }
        public RelayCommand<object> OpenTabCommand { get; set; }

        public AdminViewModel()
        {
            _authService = App.ServiceProvider.GetRequiredService<AuthService>();
            LogoutCommand = new RelayCommand<object>((obj) => LogOut(obj));
            OpenTabCommand = new RelayCommand<object>((obj) => OpenTab(obj));
        }

        public void LogOut(object obj)
        {
            T.DialogParameters dialogParameters = new T.DialogParameters
            {
                Header = Resources.UI.Logout,
                Content = Resources.Messages.AreYouSure,
                Closed = OnClosed
            };

            if (obj is MainWindow)
            {
                dialogParameters.Owner = (MainWindow)obj;
                dialogParameters.DialogStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            }

            T.RadWindow.Confirm(dialogParameters);
        }

        private void OnClosed(object sender, T.WindowClosedEventArgs e)
        {
            var result = e.DialogResult;

            if (result == true)
                if (_authService.Logout())
                    MessengerInstance.Send<NotifocationModel, MainViewModel>(new NotifocationModel { ViewType = ViewType.LoginView });
        }

        public void OpenTab(object obj)
        {
            ViewType viewModelType = (ViewType)obj;

            switch (viewModelType)
            {
                case ViewType.AdminView:
                    break;

                case ViewType.CashboxesView:
                    break;

                case ViewType.CashboxView:
                    break;

                case ViewType.CashOperationsView:
                    break;

                case ViewType.CommonSettingsView:
                    break;

                case ViewType.CoastsView:
                    break;

                case ViewType.CurrencyView:
                    break;

                case ViewType.EditEndpointView:
                    break;

                case ViewType.ExpancesView:
                    break;

                case ViewType.InvoicesView:
                    break;

                case ViewType.LeftoversView:
                    break;

                case ViewType.LoginView:
                    break;

                case ViewType.MovementsView:
                    break;

                case ViewType.OrdersView:
                    break;

                case ViewType.PlacesView:
                    break;

                case ViewType.ProductsView:
                    break;

                case ViewType.SalesView:
                    break;

                case ViewType.StaffView:
                    break;

                case ViewType.UsersView:
                    break;

                case ViewType.WarehousesView:
                    break;

                case ViewType.WriteOffsView:
                    break;

                default:
                    break;
            }
        }
    }
}
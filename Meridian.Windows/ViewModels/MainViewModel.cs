﻿using GalaSoft.MvvmLight;
using Meridian.Windows.Models;
using Meridian.Windows.Views;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private UserControl _controlView;

        public UserControl ControlView
        {
            get => _controlView;
            set => Set(ref _controlView, value);
        }

        public MainViewModel()
        {
            ControlView = new LoginView();
            MessengerInstance.Register<NotifocationModel>(this, UpdateControlView);
        }

        public void UpdateControlView(NotifocationModel notifocationModel)
        {
            switch (notifocationModel.ViewType)
            {
                case ViewType.LoginView:
                    ControlView = new LoginView();
                    break;

                case ViewType.AdminView:
                    ControlView = new AdminView();
                    break;

                default:
                    break;
            }
        }
    }
}
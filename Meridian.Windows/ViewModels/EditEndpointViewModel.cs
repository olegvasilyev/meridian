﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Meridian.Models.Models;
using Meridian.Windows.Data;
using Meridian.Windows.Models;
using Meridian.Windows.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Meridian.Windows.ViewModels
{
    public class EditEndpointViewModel : ViewModelBase
    {
        private readonly ChecksService _checkService;
        private readonly AppDbContext _context;
        private readonly RestBase _rest;
        private string _endPoint;
        private string _result;

        public string EndPoint
        {
            get => _endPoint;
            set
            {
                Set(ref _endPoint, value);
                CheckCommand.RaiseCanExecuteChanged();
            }
        }

        public string Result
        {
            get => _result;
            set => Set(ref _result, value);
        }

        public RelayCommand CheckCommand { get; set; }
        public RelayCommand ApplyCommand { get; set; }

        public EditEndpointViewModel()
        {
            _checkService = App.ServiceProvider.GetRequiredService<ChecksService>();
            _context = App.ServiceProvider.GetRequiredService<AppDbContext>();
            _rest = App.ServiceProvider.GetRequiredService<RestBase>();
            Result = Resources.Messages.EndpointDialogHint;

            _endPoint = _context.Settings.FirstOrDefault(x => x.SettingCode.Equals(SettingCode.ApiEndPoint))?.Value;

            CheckCommand = new RelayCommand(async () => await CheckEndpointAsync(), () => ValidateUrl());
            ApplyCommand = new RelayCommand(async () => await ApplyEndpointAsync());
        }

        private async Task CheckEndpointAsync()
        {
            Result = Resources.Messages.Testing;

            var response = await _checkService.CheckEndpointAsync(_endPoint);

            switch (response.ResultStatus)
            {
                case ResultStatus.Success:
                    Result = $"OK! API version: {response.Message}";
                    break;

                case ResultStatus.Warning:
                    Result = response.Message;
                    break;

                case ResultStatus.Error:
                    Result = response.Message;
                    break;

                default:
                    break;
            }
        }

        private async Task ApplyEndpointAsync()
        {
            try
            {
                var endPoint = await _context.Settings.FirstOrDefaultAsync(x => x.SettingCode.Equals(SettingCode.ApiEndPoint));

                if (endPoint?.Value != null)
                {
                    if (!endPoint.Value.Equals(_endPoint))
                    {
                        endPoint.Value = _endPoint;
                        endPoint.Modified = DateTime.Now.Ticks;
                        await _context.SaveChangesAsync();
                    }
                }
                else
                {
                    _context.Settings.Add(new Setting
                    {
                        SettingCode = SettingCode.ApiEndPoint,
                        Code = SettingCode.ApiEndPoint.ToString(),
                        Value = _endPoint,
                        ValueType = typeof(string).ToString(),
                        Description = "Meridian API endpoint"
                    });

                    await _context.SaveChangesAsync();
                }

                _rest.ApiEndPoint = _endPoint;

                Result = Resources.Messages.SuccessStoreSettings;
            }
            catch (Exception)
            {
                Result = string.Format(Resources.Messages.ErrorStoreSettings, App.DBFILENAME);
            }
        }

        private bool ValidateUrl()
        {
            return Uri.IsWellFormedUriString(_endPoint, UriKind.Absolute);
        }
    }
}
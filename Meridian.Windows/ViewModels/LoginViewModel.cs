﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Meridian.Models.Helpers;
using Meridian.Models.Models;
using Meridian.Windows.Data;
using Meridian.Windows.Models;
using Meridian.Windows.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Meridian.Windows.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private string _status;
        private string _login;
        private string _password;

        public string Status
        {
            get => _status;
            set => Set(ref _status, value);
        }

        public string Login
        {
            get => _login;
            set
            {
                Set(ref _login, value);
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                Set(ref _password, value);
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        private readonly AuthService _authService;
        private readonly AppDbContext _context;

        public RelayCommand<object> LoginCommand { get; }

        public LoginViewModel()
        {
            _authService = App.ServiceProvider.GetRequiredService<AuthService>();
            _context = App.ServiceProvider.GetRequiredService<AppDbContext>();
            LoginCommand = new RelayCommand<object>(async (obj) => await LoginAsync(obj), (obj) => CredentialsIsAccepted());
            Status = Resources.UI.LoginDefaulStatus;
        }

        private bool CredentialsIsAccepted() => (_login ?? string.Empty).Length > 3 && (_password ?? string.Empty).Length > 3;

        private async Task LoginAsync(object obj)
        {
            ResetStatusBar(Resources.UI.LogingProcess);

            var passwordBox = (Telerik.Windows.Controls.RadPasswordBox)obj;
            string password = string.Empty;

            if (passwordBox != null)
            {
                password = passwordBox.Password;
            }

            UserLoginModel userLoginModel = new UserLoginModel { UserName = Login, Password = password };

            try
            {
                var response = await _authService.LoginAsync(userLoginModel);

                switch (response.ResultStatus)
                {
                    case ResultStatus.Success:
                        App.Context = new AuthenticationContext
                        {
                            UserName = response.Data.User.UserName,
                            TimeZoneID = TimeZoneInfo.Local.Id,
                            Culture = response.Data.Culture,
                            Currency = response.Data.Currency
                        };

                        var culture = await _context.Settings.FirstOrDefaultAsync(x => x.SettingCode.Equals(SettingCode.UICulture));

                        if (culture != null)
                        {
                            if (!culture.Value.Equals(response.Data.Culture))
                            {
                                culture.Value = response.Data.Culture;
                                await _context.SaveChangesAsync();

                                App.SetUICulture(response.Data.Culture);
                            }
                        }
                        else
                        {
                            _context.Settings.Add(new Setting
                            {
                                Code = SettingCode.UICulture.ToString(),
                                SettingCode = SettingCode.UICulture,
                                Modified = DateTime.Now.Ticks,
                                Value = response.Data.Culture,
                                ValueType = typeof(string).ToString(),
                                Description = "UI culture"
                            });

                            await _context.SaveChangesAsync();

                            App.SetUICulture(response.Data.Culture);
                        }

                        Login = string.Empty;
                        Password = string.Empty;

                        var roles = response.Data.User.RoleDtos;

                        if (response.Data.User.RoleDtos.Count == 1 && response.Data.User.RoleDtos.FirstOrDefault(x => x.Name.Contains(Constants.CASHIER)) != null)
                        {
                            MessengerInstance.Send<NotifocationModel, MainViewModel>(new NotifocationModel { ViewType = ViewType.CashboxView });
                        }
                        else if (response.Data.User.RoleDtos.Count == 1 && response.Data.User.RoleDtos.FirstOrDefault(x => x.Name.Contains(Constants.CUSTOMER)) != null)
                        {
                            Status = Resources.Messages.ErrorCustomerLogin;
                        }
                        else
                        {
                            MessengerInstance.Send<NotifocationModel, MainViewModel>(new NotifocationModel { ViewType = ViewType.AdminView });
                        }

                        ResetStatusBar();
                        break;

                    case ResultStatus.Warning:
                        Status = response.Message;
                        break;

                    case ResultStatus.Error:
                        Status = response.Message;
                        break;

                    default:
                        break;
                }
            }
            catch (Exception)
            {
                Status = Resources.Messages.ErronNetwork;
            }
        }

        private void ResetStatusBar(string status = null)
        {
            Status = status ?? Resources.UI.LoginDefaulStatus;
        }
    }
}
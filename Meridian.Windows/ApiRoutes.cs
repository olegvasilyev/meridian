﻿namespace Meridian.Windows
{
    public static class ApiRoutes
    {
        public const string LOGIN = "api/auth/login";
        public const string CHECKVERSION = "api/checks/version";
    }
}
﻿using Meridian.Models.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Tiny.RestClient;

namespace Meridian.Windows.Services
{
    /// <summary>
    /// ChecksService performs check requests to API server
    /// </summary>
    public class ChecksService
    {
        /// <summary>
        /// Check API server version, otherwise return ResultStatus.Error and error message
        /// </summary>
        /// <param name="endPoint">Ulr or IP of API endpoint</param>
        /// <returns></returns>
        public async Task<ResultBase<Version>> CheckEndpointAsync(string endPoint)
        {
            ResultBase<Version> result = new ResultBase<Version> { Data = null };
            string body;

            try
            {
                var client = new TinyRestClient(new HttpClient(), endPoint);
                var response = await client.GetRequest(ApiRoutes.CHECKVERSION).ExecuteAsHttpResponseMessageAsync();

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        body = await response.Content.ReadAsStringAsync();
                        result.Message = JsonConvert.DeserializeObject<string>(body);
                        result.ResultStatus = ResultStatus.Success;
                        break;

                    case HttpStatusCode.BadRequest:
                        body = await response.Content.ReadAsStringAsync();
                        result.SetError(JsonConvert.DeserializeObject<string>(body));
                        break;

                    default:
                        break;
                }
            }
            catch (Exception)
            {
                result.Message = Resources.Messages.ErronNetwork;
            }

            return result;
        }
    }
}
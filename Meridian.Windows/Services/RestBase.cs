﻿using Meridian.Windows.Data;
using Meridian.Windows.Models;
using System.Linq;
using System.Net.Http;
using Tiny.RestClient;

namespace Meridian.Windows.Services
{
    public class RestBase
    {
        private string _endPoint;

        public TinyRestClient Client { get; private set; }

        public string ApiEndPoint
        {
            get => _endPoint;
            set
            {
                if (!_endPoint.Equals(value))
                {
                    _endPoint = value;
                    Client = new TinyRestClient(new HttpClient(), value);
                }
            }
        }

        public RestBase(AppDbContext context)
        {
            _endPoint = context.Settings.FirstOrDefault(x => x.SettingCode.Equals(SettingCode.ApiEndPoint))?.Value ?? string.Empty;
            Client = new TinyRestClient(new HttpClient(), _endPoint);
        }
    }
}
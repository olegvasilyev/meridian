﻿using Meridian.Models.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Meridian.Windows.Services
{
    public class AuthService
    {
        private readonly RestBase _rest;

        public AuthService(RestBase restBase)
        {
            _rest = restBase;
        }

        public async Task<ResultBase<LoginResultModel>> LoginAsync(UserLoginModel userLoginModel)
        {
            ResultBase<LoginResultModel> result = new ResultBase<LoginResultModel> { Data = null };
            string body;

            try
            {
                var response = await _rest.Client.PostRequest(ApiRoutes.LOGIN, userLoginModel).ExecuteAsHttpResponseMessageAsync();

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        body = await response.Content.ReadAsStringAsync();
                        result.Data = JsonConvert.DeserializeObject<LoginResultModel>(body);
                        result.ResultStatus = ResultStatus.Success;
                        _rest.Client.Settings.DefaultHeaders.AddBearer(result.Data.Token);
                        break;

                    case HttpStatusCode.Unauthorized:
                        body = await response.Content.ReadAsStringAsync();
                        result.SetError(JsonConvert.DeserializeObject<string>(body));
                        break;

                    case HttpStatusCode.NotFound:
                        body = await response.Content.ReadAsStringAsync();
                        result.SetWarning(JsonConvert.DeserializeObject<string>(body));
                        break;

                    case HttpStatusCode.BadRequest:
                        body = await response.Content.ReadAsStringAsync();
                        result.SetError(JsonConvert.DeserializeObject<string>(body));
                        break;

                    default:
                        break;
                }
            }
            catch (Exception)
            {
                result.Message = Resources.Messages.ErronNetwork;
            }

            return result;
        }

        public bool Logout()
        {
           return _rest.Client.Settings.DefaultHeaders.Remove("Authorization");
        }
    }
}
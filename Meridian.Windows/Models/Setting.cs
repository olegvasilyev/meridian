﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Meridian.Windows.Models
{
    public class Setting
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public long Modified { get; set; }

        [Required]
        [MaxLength(16)]
        public string Code { get; set; }

        [Required]
        public SettingCode SettingCode { get; set; }

        [MaxLength(128)]
        [Required]
        public string Value { get; set; }

        [MaxLength(64)]
        [Required]
        public string ValueType { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public object GetInstance()
        {
            Type type = Type.GetType(ValueType);
            if (type != null)
                return Activator.CreateInstance(type);
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = asm.GetType(ValueType);
                if (type != null)
                    return Activator.CreateInstance(type);
            }
            return null;
        }

        public DateTime GetDateTime()
        {
            return new DateTime(Modified, DateTimeKind.Local);
        }
    }

    public enum SettingCode
    {
        ApiEndPoint,
        UICulture,
        UITheme
    }
}
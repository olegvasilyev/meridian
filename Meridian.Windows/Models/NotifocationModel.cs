﻿using GalaSoft.MvvmLight.Messaging;

namespace Meridian.Windows.Models
{
    public class NotifocationModel : MessageBase
    {
        public ViewType ViewType { get; set; }
    }

    public enum ViewType
    {
        AdminView,
        CashboxesView,
        CashboxView,
        CashOperationsView,
        CommonSettingsView,
        CoastsView,
        CurrencyView,
        EditEndpointView,
        ExpancesView,
        InvoicesView,
        LeftoversView,
        LoginView,
        MovementsView,
        OrdersView,
        PlacesView,
        ProductsView,
        SalesView,
        StaffView,
        UsersView,
        WarehousesView,
        WriteOffsView
    }
}
﻿using Newtonsoft.Json;
using System.Net;

namespace Meridian.Windows.Models
{
    public class ResponseBase
    {
        public HttpStatusCode Code { get; set; }

        public bool IsSuccess { get; set; }
        public string Body { get; set; }

        public TResult DeserializeBody<TResult>()
        {
            return JsonConvert.DeserializeObject<TResult>(Body);
        }
    }
}
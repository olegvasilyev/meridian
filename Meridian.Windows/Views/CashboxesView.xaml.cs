﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class CashboxesView : UserControl
    {
        public CashboxesView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<CashboxesViewModel>();
        }
    }
}
﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class CashOperationsView : UserControl
    {
        public CashOperationsView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<CashOperationsViewModel>();
        }
    }
}
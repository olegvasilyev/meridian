﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class ExpancesView : UserControl
    {
        public ExpancesView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<ExpancesViewModel>();
        }
    }
}
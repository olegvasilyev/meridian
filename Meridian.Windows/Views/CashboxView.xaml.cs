﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class CashboxView : UserControl
    {
        public CashboxView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<CashboxViewModel>();
        }
    }
}
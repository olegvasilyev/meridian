﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class EmployeesView : UserControl
    {
        public EmployeesView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<EmploeesViewModel>();
        }
    }
}
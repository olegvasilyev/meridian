﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class PlacesView : UserControl
    {
        public PlacesView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<PlacesViewModel>();
        }
    }
}
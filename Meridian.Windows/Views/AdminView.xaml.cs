﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class AdminView : UserControl
    {
        public AdminView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<AdminViewModel>();
            InitializeComponent();
        }
    }
}
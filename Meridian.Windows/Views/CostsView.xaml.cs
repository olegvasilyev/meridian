﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class CostsView : UserControl
    {
        public CostsView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<CostsViewModel>();
        }
    }
}
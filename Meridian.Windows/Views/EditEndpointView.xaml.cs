﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class EditEndpointView : RadWindow
    {
        public EditEndpointView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<EditEndpointViewModel>();
            InitializeComponent();

            this.KeyDown += new KeyEventHandler(EditEndpointView_KeyDown);
        }

        private void EditEndpointView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }
    }
}
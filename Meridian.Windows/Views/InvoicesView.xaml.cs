﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class InvoicesView : UserControl
    {
        public InvoicesView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<InvoicesViewModel>();
            InitializeComponent();
        }
    }
}
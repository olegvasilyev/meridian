﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Meridian.Windows.Views
{
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            var vm = App.ServiceProvider.GetRequiredService<LoginViewModel>();

            DataContext = vm;
            InitializeComponent();

            var keyBinding = new KeyBinding
            {
                Command = vm.LoginCommand,
                Key = Key.Enter
            };

            keyBinding.CommandParameter = PasswordBox;

           this.InputBindings.Add(keyBinding);
        }

        private void EditSettingButton_Click(object sender, RoutedEventArgs e)
        {
            var window = new EditEndpointView() { Owner = Window.GetWindow(this) };
            window.ShowDialog();
        }
    }
}
﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class WriteOffsView : UserControl
    {
        public WriteOffsView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<WriteOffsViewModel>();
        }
    }
}
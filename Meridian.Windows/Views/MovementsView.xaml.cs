﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class MovementsView : UserControl
    {
        public MovementsView()
        {
            DataContext = App.ServiceProvider.GetRequiredService<MovementsViewModel>();
            InitializeComponent();
        }
    }
}
﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class CommonSettingsView : UserControl
    {
        public CommonSettingsView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<CommonSettingsViewModel>();
        }
    }
}
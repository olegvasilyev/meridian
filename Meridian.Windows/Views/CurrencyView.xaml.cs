﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class CurrencyView : UserControl
    {
        public CurrencyView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<CurrencyViewModel>();
        }
    }
}
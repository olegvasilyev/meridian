﻿using Meridian.Windows.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using System.Windows.Controls;

namespace Meridian.Windows.Views
{
    public partial class WarehousesView : UserControl
    {
        public WarehousesView()
        {
            InitializeComponent();
            DataContext = App.ServiceProvider.GetRequiredService<WarehousesViewModel>();
        }
    }
}
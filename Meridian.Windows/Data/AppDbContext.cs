﻿using Meridian.Windows.Models;
using Microsoft.EntityFrameworkCore;

namespace Meridian.Windows.Data
{
    public class AppDbContext : DbContext
    {
        public DbSet<Setting> Settings { get; set; }
        public DbSet<WinState> WindowStates { get; set; }

        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
                optionsBuilder.UseSqlite($"Data Source={App.DBFILENAME}");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
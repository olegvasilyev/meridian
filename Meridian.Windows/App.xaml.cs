﻿using Meridian.Models.Models;
using Meridian.Windows.Data;
using Meridian.Windows.Helpers;
using Meridian.Windows.Models;
using Meridian.Windows.Services;
using Meridian.Windows.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace Meridian.Windows
{
    public partial class App : Application
    {
        private static readonly object _locker = new object();
        private static AuthenticationContext _context;
        public static CultureInfo UICulture { get; private set; }

        public static AuthenticationContext Context
        {
            get
            {
                lock (_locker)
                {
                    return _context ?? new AuthenticationContext();
                }
            }
            set
            {
                lock (_locker)
                {
                    _context = value;
                }
            }
        }

        public static string DBFILENAME
        {
            get
            {
                string appData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

                var appPath = Path.Combine(appData, "Meridian Client");

                if (!Directory.Exists(appPath))
                    Directory.CreateDirectory(appPath);

                return Path.Combine(appPath, "client.db");
            }
        }

        public static IServiceProvider ServiceProvider { get; private set; }

        public App()
        {
            DispatcherUnhandledException += OnDispatcherUnhandledException;

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            ServiceProvider = serviceCollection.BuildServiceProvider();

            //Telerik.Windows.Controls.FluentPalette.Palette.FontSizeS = 14;
            //Telerik.Windows.Controls.FluentPalette.Palette.FontSize = 16;
            //Telerik.Windows.Controls.FluentPalette.Palette.FontSizeL = 17;
            //Telerik.Windows.Controls.FluentPalette.Palette.FontSizeXL = 18;
        }

        public static void SetUICulture(string culture)
        {
            UICulture = new CultureInfo(culture);

            Thread.CurrentThread.CurrentCulture = UICulture;
            Thread.CurrentThread.CurrentUICulture = UICulture;

            Meridian.Windows.Resources.UI.Culture = UICulture;
            Meridian.Windows.Resources.Messages.Culture = UICulture;
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(x =>
            {
                x.UseSqlite($"Data Source={DBFILENAME}");
            });

            services.AddSingleton<RestBase>();
            services.AddTransient<AuthService>();
            services.AddTransient<ChecksService>();

            // Register all ViewModels.
            services.AddScoped<MainViewModel>();
            services.AddTransient<EditEndpointViewModel>();
            services.AddScoped<LoginViewModel>();
            services.AddScoped<AdminViewModel>();
            //services.AddTransient<CashboxesViewModel>();
            //services.AddTransient<CashboxViewModel>();
            //services.AddTransient<CashOperationsViewModel>();
            //services.AddTransient<CommonSettingsViewModel>();
            //services.AddTransient<CostsViewModel>();
            //services.AddTransient<CurrencyViewModel>();
            //services.AddTransient<ExpancesViewModel>();
            //services.AddTransient<InvoicesViewModel>();
            //services.AddTransient<LeftoversViewModel>();
            //services.AddTransient<MovementsViewModel>();
            //services.AddTransient<OrdersViewModel>();
            //services.AddTransient<PlacesViewModel>();
            //services.AddTransient<ProductsViewModel>();
            //services.AddTransient<SalesViewModel>();
            //services.AddTransient<EmploeesViewModel>();
            //services.AddTransient<UsersViewModel>();
            //services.AddTransient<WarehousesViewModel>();
            //services.AddTransient<WriteOffsViewModel>();

            // Register all the Windows of the applications.
            //services.AddTransient<MainWindow>();
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            var context = ServiceProvider.GetRequiredService<AppDbContext>();

            await context.Database.MigrateAsync();
            await AppDbSeed.SeedDataAsync(context);

            var setting = await context.Settings.FirstOrDefaultAsync(x => x.SettingCode.Equals(SettingCode.UICulture));
            var culture = setting != null ? setting.Value : CultureInfo.CurrentCulture.Name;
            SetUICulture(culture);

            base.OnStartup(e);
        }

        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(e.Exception.Message);
            sb.AppendLine();
            sb.AppendLine(e.Exception.StackTrace);
            MessageBox.Show(sb.ToString());
            e.Handled = true;
        }
    }
}